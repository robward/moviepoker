﻿namespace MoviePoker.Test.MovieDatabaseClientDriver
{
    using MoviePoker.Data.MovieDatabase;

    class Program
    {

        static void Main(string[] args)
        {
            Client movieDatabaseClient = new Client(
                "http://api.themoviedb.org/2.1/Movie.search/en/xml",
                "0df1b9eda9888caf0e804477a41f968f");
            movieDatabaseClient.MovieSearch("Transformers");
        }
    }
}
