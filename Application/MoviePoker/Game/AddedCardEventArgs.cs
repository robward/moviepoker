﻿namespace MoviePoker
{
    using System;
    
    [Serializable]
    public class AddedCardEventArgs : EventArgs
    {
        private byte cardNumber;

        public AddedCardEventArgs(byte cardNumber)
        {
            this.cardNumber = cardNumber;
        }

        public byte CardNumber
        {
            get
            {
                return this.cardNumber;
            }
        }
    }
}
