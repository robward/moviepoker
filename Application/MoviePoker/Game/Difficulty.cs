﻿namespace MoviePoker
{
    public enum Difficulty
    {
        Easy = -1,
        Average = 0,
        Hard = 1
    }
}
