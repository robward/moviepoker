﻿namespace MoviePoker
{
    using System;
    using data = MoviePoker.Data.LocalDataAccess;

    [Serializable]
    public class GameCard
    {
        data.Card card;

        protected GameCard(data.Card card)
        {
            this.card = card;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal data.Card Card
        {
            get
            {
                return this.card;
            }
        }

        public override string ToString()
        {
            return this.card.ToString();
        }
    }
}
