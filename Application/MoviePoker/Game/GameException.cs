﻿namespace MoviePoker
{
    using System;
    using System.Runtime.Serialization;
    using MoviePoker.Management;

    [Serializable]
    class GameException: ManagedException
    {
        public GameException(string message)
            : base(message)
        {
        }

        protected GameException(SerializationInfo serializationInformation, StreamingContext streamingContext)
            :base(serializationInformation, streamingContext)
        {
        }

    }
}
