﻿namespace MoviePoker
{
    using System;
    
    [Serializable]
    class NewRoundEventArgs : EventArgs
    {
        private Round round;

        internal NewRoundEventArgs(Round round)
        {
            this.round = round;
        }

        internal Round Round
        {
            get
            {
                return this.round;
            }
        }
    }
}
