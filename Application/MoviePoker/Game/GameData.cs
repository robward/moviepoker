﻿namespace MoviePoker
{
    using System;
    using System.Configuration;
    using System.Linq;
    using System.Transactions;
    using MoviePoker.Management;
    using MoviePoker.Utility;
    using client = MoviePoker.Data.MovieDatabaseClient;
    using data = MoviePoker.Data.LocalDataAccess;
    using MoviePoker.Properties;

    class GameData: IDisposable
    {
        static client.MovieDatabaseClientSection movieDatabaseClientConfigurationSection =
            ConfigurationManager.GetSection(client.MovieDatabaseClientSection.SectionName) as client.MovieDatabaseClientSection;

        data.MoviePokerEntities dataContext;

        internal GameData()
        {
            this.dataContext = new data.MoviePokerEntities();
        }

        internal data.MoviePokerEntities DataContext
        {
            get
            {
                return this.dataContext;
            }
        }
        
        static client.IMovieDatabaseClient CreateMovieDatabaseClient()
        {
            return client.ClientFactory.Create(
                GameData.movieDatabaseClientConfigurationSection.Address,
                GameData.movieDatabaseClientConfigurationSection.UserKey);
        }

        public void Dispose()
        {
            this.dataContext.Dispose();
            GC.SuppressFinalize(this);
        }

        void ProcessMovieActors(client.Movie remoteMovie, data.Movie movie)
        {
            var unknownActorQuery =
                from actorIdentifier in
                    (
                        from externalIdentifierValueItem in
                            (
                                from identifierValueItem in remoteMovie.ActorIdentifiers
                                select identifierValueItem
                            )
                        join externalIdentifierItem in this.dataContext.PersonExternalIdentifier
                            on externalIdentifierValueItem equals externalIdentifierItem.ExternalIdentifierValue
                        select externalIdentifierItem
                    ).Except
                    (
                        from movieItem in this.dataContext.Movie
                        from personExternalIdentifierItem in movieItem.MovieExternalIdentifier.MovieActorPersonExternalIdentifier
                        where movieItem.MovieKey == movie.MovieKey
                        select personExternalIdentifierItem
                    )
                select actorIdentifier;
            foreach (data.PersonExternalIdentifier unknownActorIdentifier in unknownActorQuery.Distinct())
            {
                Tracing.TraceInformation(
                    Resources.AdditionTrace, 
                    Resources.ActorArgumentString, 
                    unknownActorIdentifier.ExternalIdentifierValue);

                movie.MovieExternalIdentifier.MovieActorPersonExternalIdentifier.Add(unknownActorIdentifier);
            }
            this.dataContext.SaveChanges(true);
        }

        void ProcessMovieDirectors(client.Movie remoteMovie, data.Movie movie)
        {
            var unknownDirectorQuery =
                from directorIdentifier in
                    (
                        from externalIdentifierValueItem in
                            (
                                from identifierValueItem in remoteMovie.DirectorIdentifiers
                                select identifierValueItem
                            )
                        join externalIdentifierItem in this.dataContext.PersonExternalIdentifier
                            on externalIdentifierValueItem equals externalIdentifierItem.ExternalIdentifierValue
                        select externalIdentifierItem
                    ).Except
                    (
                        from movieItem in this.dataContext.Movie
                        from personExternalIdentifierItem in movieItem.MovieExternalIdentifier.MovieDirectorPersonExternalIdentifier
                        where movieItem.MovieKey == movie.MovieKey
                        select personExternalIdentifierItem
                    )
                select directorIdentifier;
            foreach (data.PersonExternalIdentifier unknownDirectorIdentifier in unknownDirectorQuery.Distinct())
            {
                Tracing.TraceInformation(
                    Resources.AdditionTrace, 
                    Resources.DirectorArgumentString, 
                    unknownDirectorIdentifier.ExternalIdentifierValue);

                movie.MovieExternalIdentifier.MovieDirectorPersonExternalIdentifier.Add(unknownDirectorIdentifier);
            }
            this.dataContext.SaveChanges(true);
        }

        void ProcessMoviePeople(client.Movie remoteMovie, data.Movie movie)
        {
            var unknownPersonQuery =
                    from identifierItem in
                        (
                            (
                                from directorIdentifierItem in remoteMovie.DirectorIdentifiers
                                select directorIdentifierItem
                            ).Union
                            (
                                from actorIdentifierItem in remoteMovie.ActorIdentifiers
                                select actorIdentifierItem
                            )
                        ).Except
                        (
                            from externalIdentifierItem in this.dataContext.PersonExternalIdentifier
                            select externalIdentifierItem.ExternalIdentifierValue
                        )
                    select identifierItem;
            foreach (string unknownPersonIdentifier in unknownPersonQuery.Distinct())
            {
                Tracing.TraceInformation(Resources.AdditionTrace, Resources.PersonArgumentString, unknownPersonIdentifier);

                this.dataContext.AddToPersonExternalIdentifier(unknownPersonIdentifier);
            }
            this.dataContext.SaveChanges(true);
            
            this.ProcessMovieDirectors(remoteMovie, movie);
            this.ProcessMovieActors(remoteMovie, movie);
        }

        void ProcessPersonActingCredits(client.Person remotePerson, data.Person person)
        {
            var unknownActingCreditQuery =
                from actingCreditIdentifier in
                    (
                        from externalIdentifierValueItem in
                            (
                                from identifierValueItem in remotePerson.ActingCreditMovieIdentifiers
                                select identifierValueItem
                            )
                        join externalIdentifierItem in this.dataContext.MovieExternalIdentifier
                            on externalIdentifierValueItem equals externalIdentifierItem.ExternalIdentifierValue
                        select externalIdentifierItem
                    ).Except
                    (
                        from personItem in this.dataContext.Person
                        from movieExternalIdentifierItem in personItem.PersonExternalIdentifier.ActingCreditMovieExternalIdentifier
                        where personItem.PersonKey == person.PersonKey
                        select movieExternalIdentifierItem
                    )
                select actingCreditIdentifier;
            foreach (data.MovieExternalIdentifier unknownActingCreditIdentifier in unknownActingCreditQuery.Distinct())
            {
                Tracing.TraceInformation(
                    Resources.AdditionTrace, 
                    Resources.ActingCreditArgumentString, 
                    unknownActingCreditIdentifier.ExternalIdentifierValue);

                person.PersonExternalIdentifier.ActingCreditMovieExternalIdentifier.Add(unknownActingCreditIdentifier);
            }
            this.dataContext.SaveChanges(true);
        }

        void ProcessPersonDirectingCredits(client.Person remotePerson, data.Person person)
        {
            var unknownDirectingCreditQuery =
                from directingCreditIdentifier in
                    (
                        from externalIdentifierValueItem in
                            (
                                from identifierValueItem in remotePerson.DirectingCreditMovieIdentifiers
                                select identifierValueItem
                            )
                        join externalIdentifierItem in this.dataContext.MovieExternalIdentifier
                            on externalIdentifierValueItem equals externalIdentifierItem.ExternalIdentifierValue
                        select externalIdentifierItem
                    ).Except
                    (
                        from personItem in this.dataContext.Person
                        from movieExternalIdentifierItem in personItem.PersonExternalIdentifier.DirectingCreditMovieExternalIdentifier
                        where personItem.PersonKey == person.PersonKey
                        select movieExternalIdentifierItem
                    )
                select directingCreditIdentifier;
            foreach (data.MovieExternalIdentifier unknownDirectingCreditIdentifier in unknownDirectingCreditQuery.Distinct())
            {
                Tracing.TraceInformation(
                    Resources.AdditionTrace, 
                    Resources.DirectingCreditArgumentString, 
                    unknownDirectingCreditIdentifier.ExternalIdentifierValue);

                person.PersonExternalIdentifier.DirectingCreditMovieExternalIdentifier.Add(unknownDirectingCreditIdentifier);
            }
            this.dataContext.SaveChanges(true);
        }

        void ProcessPersonMovies(client.Person remotePerson, data.Person person)
        {
            var unknownMovieQuery =
                    from identifierItem in
                        (
                            (
                                from directingCreditIdentifierItem in remotePerson.DirectingCreditMovieIdentifiers
                                select directingCreditIdentifierItem
                            ).Union
                            (
                                from actingCreditIdentifierItem in remotePerson.ActingCreditMovieIdentifiers
                                select actingCreditIdentifierItem
                            )
                        ).Except
                        (
                            from externalIdentifierItem in this.dataContext.MovieExternalIdentifier
                            select externalIdentifierItem.ExternalIdentifierValue
                        )
                    select identifierItem;
            foreach (string unknownMovieIdentifier in unknownMovieQuery.Distinct())
            {
                Tracing.TraceInformation(Resources.AdditionTrace, Resources.MovieArgumentString, unknownMovieIdentifier);

                this.dataContext.AddToMovieExternalIdentifier(unknownMovieIdentifier);
                this.dataContext.SaveChanges(true);
            }
            
            this.ProcessPersonActingCredits(remotePerson, person);
            this.ProcessPersonDirectingCredits(remotePerson, person);
        }

        internal bool TryRetrieveMovie(string externalIdentifier, out data.Movie movie)
        {
            Tracing.TraceInformation(Resources.RetrievalTrace, Resources.MovieArgumentString, externalIdentifier);

            movie = null;
            client.Movie remoteMovie = null;
            
            if (!(GameData.TryRetrieveMovie(externalIdentifier, out remoteMovie)))
            {
                return false;
            }
            
            using (TransactionScope transaction = data.Utility.InitializeTransaction())
            {
                long? revenue = remoteMovie.Revenue;

                var movieQuery =
                    from movieItem in this.dataContext.Movie
                    where movieItem.MovieExternalIdentifier.ExternalIdentifierValue.Equals(
                        externalIdentifier,
                        StringComparison.OrdinalIgnoreCase)
                    select movieItem;
                movie = movieQuery.FirstOrDefault();
                if (movie != null)
                {
                    if ((revenue.HasValue) && (movie.WorldwideRevenue != revenue.Value))
                    {
                        movie.WorldwideRevenue = revenue.Value;
                    }
                }
                else
                {
                    DateTime? releaseDate = remoteMovie.ReleaseDate;
                    if (!(releaseDate.HasValue))
                    {
                        return false;
                    }

                    movie = this.dataContext.AddToMovie(
                        remoteMovie.Title,
                        externalIdentifier,
                        (short)remoteMovie.ReleaseDate.Value.Year,
                        revenue.HasValue ? revenue.Value : 0);

                }
                movie.Load();

                this.ProcessMoviePeople(remoteMovie, movie);
                transaction.Complete();
            }
            
            return true;
        }

        static bool TryRetrieveMovie(string externalIdentifier, out client.Movie movie)
        {
            movie = null;

            using (client.IMovieDatabaseClient movieDatabaseClient = GameData.CreateMovieDatabaseClient())
            {
                try
                {
                    movie = movieDatabaseClient.EndGetMovie(
                        movieDatabaseClient.BeginGetMovie(externalIdentifier, null));
                    return true;
                }
                catch (ManagedException)
                {
                    return false;
                }
                catch (Exception exception)
                {
                    Tracing.TraceError(exception);

                    if (ExceptionAnalyzer.IsFatal(exception))
                    {
                        throw;
                    }

                    return false;
                }
            }
        }

        internal bool TryRetrievePerson(string externalIdentifier, out data.Person person)
        {
            Tracing.TraceInformation(Resources.RetrievalTrace, Resources.PersonArgumentString, externalIdentifier);

            person = null;
            client.Person remotePerson = null;
            
            if (!(GameData.TryRetrievePerson(externalIdentifier, out remotePerson)))
            {
                return false;
            }

            using (TransactionScope transaction = data.Utility.InitializeTransaction())
            {
                var personQuery =
                    from personItem in this.dataContext.Person
                    where personItem.PersonExternalIdentifier.ExternalIdentifierValue.Equals(
                        externalIdentifier,
                        StringComparison.OrdinalIgnoreCase)
                    select personItem;
                person = personQuery.FirstOrDefault();
                if (null == person)
                {
                    person = this.dataContext.AddToPerson(
                        remotePerson.Name,
                        remotePerson.Identifier);
                }
                
                this.ProcessPersonMovies(remotePerson, person);
                transaction.Complete();
            }
            
            return true;
        }


        internal static bool TryRetrievePerson(string externalIdentifier, out client.Person person)
        {
            person = null;

            using (client.IMovieDatabaseClient movieDatabaseClient = GameData.CreateMovieDatabaseClient())
            {
                try
                {
                    person = movieDatabaseClient.EndGetPerson(
                        movieDatabaseClient.BeginGetPerson(externalIdentifier, null));
                    return true;
                }
                catch (ManagedException)
                {
                    return false;
                }
                catch (Exception exception)
                {
                    Tracing.TraceError(exception);

                    if (ExceptionAnalyzer.IsFatal(exception))
                    {
                        throw;
                    }

                    return false;
                }
            }
        }

        
    }
}
