﻿namespace MoviePoker
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using MoviePoker.Management;
    using MoviePoker.Properties;

    [Serializable]
    class Round
    {
        const byte DefaultCommunityCardLimit = 5;
        const byte DefaultFlopCount = 3;
        const byte DefaultPlayerCardLimit = 2;

        [NonSerialized]
        readonly Shoe shoe;

        byte communityCardLimit;
        byte flopCount;
        byte playerCardLimit;

        bool last;

        GameCard[] communityCards;
        GameCard[][] playerCards;

        internal Round(GameDeck deck, byte playerCount)
            :this(
                deck, 
                playerCount, 
                Round.DefaultCommunityCardLimit, 
                Round.DefaultFlopCount, 
                Round.DefaultPlayerCardLimit)
        {
        }
        
        internal Round(
            GameDeck deck, 
            byte playerCount, 
            byte communityCardLimit,
            byte flopCount,
            byte playerCardLimit)
        {
            if (flopCount > communityCardLimit)
            {
                throw new ManagedException(Resources.FlopCountException);
            }

            this.communityCardLimit = communityCardLimit;
            this.flopCount = flopCount;

            this.playerCardLimit = playerCardLimit;

            this.shoe = new Shoe(deck);

            this.communityCards = new GameCard[this.communityCardLimit];
            this.playerCards = new GameCard[playerCount][];
            for (int playerIndex = 0; playerIndex < playerCount; playerIndex++)
            {
                this.playerCards[playerIndex] = new GameCard[this.playerCardLimit];
            }

            this.Deal();
        }

        internal ReadOnlyCollection<GameCard> CommunityCards
        {
            get;
            private set;
        }

        internal ReadOnlyCollection<GameCard[]> PlayerCards
        {
            get;
            private set;
        }

        internal bool Last
        {
            get
            {
                return this.last;
            }

            set
            {
                if (!(this.last))
                {
                    this.last = value;
                }
            }

        }

        void BurnCard()
        {
            this.shoe.DealCard();
        }

        void Deal()
        {
            this.BurnCard();
            
            for (byte playerCardIndex = 0; playerCardIndex < this.playerCardLimit; playerCardIndex++)
            {
                this.DealToPlayers(playerCardIndex);
            }
            
            this.BurnCard();

            for (byte flopCardIndex = 0; flopCardIndex < this.flopCount; flopCardIndex++)
            {
                this.communityCards[flopCardIndex] = this.shoe.DealCard();
            }

            if (this.communityCardLimit > this.flopCount)
            {
                for (byte communityCardIndex = this.flopCount; communityCardIndex < this.communityCardLimit; communityCardIndex++)
                {
                    this.BurnCard();
                    this.communityCards[communityCardIndex] = this.shoe.DealCard();
                }
            }

            this.CommunityCards = new ReadOnlyCollection<GameCard>(this.communityCards);
            this.PlayerCards = new ReadOnlyCollection<GameCard[]>(this.playerCards);
        }

        void DealToPlayers(byte cardNumber)
        {
            int playerCount = this.playerCards.Length;
            for (byte playerIndex = 0; playerIndex < playerCount; playerIndex++)
            {
                this.playerCards[playerIndex][cardNumber] = this.shoe.DealCard();
            }
        }

        class Shoe
        {
            readonly Queue<GameCard> shuffledDeck;

            internal Shoe(GameDeck deck)
            {
                this.shuffledDeck = new Queue<GameCard>(deck.ShuffledCards);
            }

            internal GameCard DealCard()
            {
                if (this.shuffledDeck.Count <= 0)
                {
                    throw new ManagedException(Resources.InsufficientCardsException);
                }

                return this.shuffledDeck.Dequeue();
            }
        }
    }
}
