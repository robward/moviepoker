﻿namespace MoviePoker
{
    using System;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Text;
    using MoviePoker.Management;
    using MoviePoker.Properties;
    
    public class Game
    {
        const byte MaximumPlayerCount = 8;
        
        static Random randomizer = new Random();

        readonly GameDeck deck;
        readonly Player[] players;

        Round currentRound;

        byte playerCount;

        internal event EventHandler<NewRoundEventArgs> NewRound;

        public Game()
            :this(
                Difficulty.Average,
                new string[] 
                { 
                    1.ToString(CultureInfo.CurrentCulture), 
                    2.ToString(CultureInfo.CurrentCulture), 
                    3.ToString(CultureInfo.CurrentCulture), 
                    4.ToString(CultureInfo.CurrentCulture) 
                })
        {
        }

        public Game(Difficulty difficulty, string[] playerNames)
        {
            this.playerCount = (byte)playerNames.Length;
            if (this.playerCount > Game.MaximumPlayerCount)
            {
                throw new ManagedException(Resources.MaximumPlayerCountExceededException);
            }

            this.deck = new GameDeck(difficulty);
            this.deck.AddedCard += new EventHandler<AddedCardEventArgs>(deck_AddedCard);

            byte[] dealingOrderArray = new byte[this.playerCount];
            for (byte index = 0; index < this.playerCount; index++)
            {
                dealingOrderArray[index] = index;
            }
            byte[] byteArray = new byte[this.playerCount];
            Game.Randomizer.NextBytes(byteArray);
            Array.Sort(byteArray, dealingOrderArray);

            this.players = new Player[this.playerCount];
            for (int playerIndex = 0; playerIndex < this.playerCount; playerIndex++)
            {
                this.players[playerIndex] = new Player(playerNames[playerIndex], dealingOrderArray[playerIndex]);
            }

            GameDeck.EndPreparingDeck(deck.BeginPreparingDeck(null));
        }

        public Difficulty Difficulty
        {
            get
            {
                return this.deck.Difficulty;
            }
        }

        internal static Random Randomizer
        {
            get
            {
                return Game.randomizer;
            }
        }

        void deck_AddedCard(object sender, AddedCardEventArgs argument)
        {
            Tracing.TraceInformation(
                string.Format(
                    CultureInfo.CurrentCulture, 
                    Resources.CardAdditionTraceformatter, 
                    argument.CardNumber));
        }

        void AdvanceDealingOrder()
        {
            Player player;
            byte lastPosition = (byte)(this.playerCount - 1);
            for (byte playerIndex = 0; playerIndex < this.playerCount; playerIndex++)
            {
                player = this.players[playerIndex];
                if (player.DealingOrderPosition == lastPosition)
                {
                    player.DealingOrderPosition = 0;
                }
                else
                {
                    player.DealingOrderPosition++;
                }
            }
        }

        internal void Play()
        {
            Player player = null;
            while (true)
            {
                this.currentRound = new Round(this.deck, this.playerCount);

                for (byte playerIndex = 0; playerIndex < playerCount; playerIndex++)
                {
                    player = this.players[playerIndex];
                    player.Hand = new ReadOnlyCollection<GameCard>(this.currentRound.PlayerCards[player.DealingOrderPosition]);
                }

                if (this.NewRound != null)
                {
                    this.NewRound(this, new NewRoundEventArgs(this.currentRound));
                }

                Tracing.TraceInformation(
                    string.Format(
                        CultureInfo.CurrentCulture,
                        Resources.NewRoundTraceFormatter,
                        string.Concat(
                            Environment.NewLine,
                            this.ToString())));

                if (this.currentRound.Last)
                {
                    break;
                }

                this.AdvanceDealingOrder();
            }
        }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
            foreach (Player player in this.players)
            {
                result.AppendLine(player.ToString());
            }
            
            result.AppendLine(Resources.CommunityCardsFormatter);
            foreach (GameCard communityCard in this.currentRound.CommunityCards)
            {
                result.AppendLine(communityCard.ToString());
            }

            return result.ToString();
        }
    }
}
