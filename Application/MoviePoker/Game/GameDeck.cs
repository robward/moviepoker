﻿namespace MoviePoker
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using MoviePoker.Management;
    using MoviePoker.Properties;
    using MoviePoker.Utility;
    using data = MoviePoker.Data.LocalDataAccess;

    public class GameDeck
    {
        const byte DeckSize = 52;
        const byte MovieSelectionAttemptsLimit = 5;

        const string ResultArgumentName = "result";
        const string StateArgumentName = "state";

        const string StringFormatter = "{0}: {1}";
                
        data.Deck deck;
        ReadOnlyCollection<GameCard> gameCards;
        readonly Difficulty difficulty = Difficulty.Average;

        internal event EventHandler<AddedCardEventArgs> AddedCard;

        internal GameDeck(Difficulty difficulty)
        {
            this.difficulty = difficulty;
        }

        internal ReadOnlyCollection<GameCard> ShuffledCards
        {
            get
            {
                if (null == this.gameCards)
                {
                    lock (this)
                    {
                        if (null == this.gameCards)
                        {
                            this.DoPrepareDeck();
                        }
                    }
                }
                this.Shuffle();
                return this.gameCards;
            }
        }

        internal Difficulty Difficulty
        {
            get
            {
                return this.difficulty;
            }
        }

        int DifficultyQuotient
        {
            get
            {
                Array levels = Enum.GetValues(typeof(Difficulty));
                int offset = 1;
                int levelLimit = levels.Length - offset;
                int index = levelLimit;
                while (index > 0)
                {
                    if ((Difficulty)levels.GetValue(index) == this.difficulty)
                    {
                        break;
                    }
                    index--;
                }
                return (levelLimit + offset) - index;
            }
        }

        internal IAsyncResult BeginPreparingDeck(AsyncCallback callback)
        {
            AsynchronousResult result = new AsynchronousResult(callback, null);
            ThreadPool.QueueUserWorkItem(new WaitCallback(this.PrepareDeck), result);
            return result;
        }

        internal static void EndPreparingDeck(IAsyncResult result)
        {
            AsynchronousResult typedResult = result as AsynchronousResult;
            if (null == typedResult)
            {
                throw new ManagedException(new ArgumentNullException(GameDeck.ResultArgumentName));
            }

            typedResult.EndInvoke();
        }

        void cardBuffer_AddedCard(object sender, AddedCardEventArgs arguments)
        {
            if (this.AddedCard != null)
            {
                this.AddedCard(this, arguments);
            }
        }

        void DoPrepareDeck()
        {
            lock (this)
            {
                if (null != this.gameCards)
                {
                    return;
                }

                CardBuffer cardBuffer = new CardBuffer();
                cardBuffer.AddedCard += new EventHandler<AddedCardEventArgs>(cardBuffer_AddedCard);
                int difficultyQuotient = this.DifficultyQuotient;

                using (GameDeckData gameDeckData = new GameDeckData())
                {
                    this.deck = gameDeckData.DataContext.AddToDeck();

                    GameCard currentCard = null;
                    data.Movie movie = null;
                    MovieCard movieCard = null;
                    data.Person[] people = null;
                    PersonCard personCard = null;
                    YearCard yearCard = null;
                    data.YearTopGrossingMovie yearTopGrossingMovie = null;

                    while (cardBuffer.Count < GameDeck.DeckSize)
                    {
                        if (!(cardBuffer.TryDequeue(out currentCard)))
                        {
                            Tracing.TraceInformation(Resources.DeckCreationRandomSelectionTrace, this.deck);
                            movie = gameDeckData.RandomlySelectMovie(difficultyQuotient, this.deck);
                            cardBuffer.AddCard(
                                new MovieCard(
                                    gameDeckData.DataContext.AddMovieCardToDeck(
                                        this.deck,
                                        movie),
                                    movie));
                        }
                        else
                        {
                            movieCard = currentCard as MovieCard;
                            if
                            (
                                (movieCard != null) &&
                                (gameDeckData.TryProcessMovieCard(movieCard, this.deck, out people, out yearTopGrossingMovie))
                            )
                            {
                                if (people != null)
                                {
                                    foreach (data.Person currentPerson in people)
                                    {
                                        if (cardBuffer.Count >= GameDeck.DeckSize)
                                        {
                                            break;
                                        }

                                        cardBuffer.AddCard(
                                            new PersonCard(
                                                gameDeckData.DataContext.AddPersonCardToDeck(
                                                    this.deck,
                                                    currentPerson),
                                                currentPerson));
                                    }
                                }

                                if ((yearTopGrossingMovie != null)&&(cardBuffer.Count < GameDeck.DeckSize))
                                {
                                    cardBuffer.AddCard(
                                        new YearCard(
                                            gameDeckData.DataContext.AddYearCardToDeck(
                                                this.deck,
                                                yearTopGrossingMovie),
                                            yearTopGrossingMovie));
                                }
                            }
                            else
                            {
                                personCard = currentCard as PersonCard;
                                if ((personCard != null) && (gameDeckData.TryProcessPersonCard(personCard, deck, out movie)))
                                {
                                    cardBuffer.AddCard(
                                        new MovieCard(
                                            gameDeckData.DataContext.AddMovieCardToDeck(
                                                this.deck,
                                                movie),
                                            movie));
                                }
                                else
                                {
                                    yearCard = currentCard as YearCard;
                                    if ((yearCard != null) && (gameDeckData.TryProcessYearCard(yearCard, deck, out movie)))
                                    {
                                        cardBuffer.AddCard(
                                            new MovieCard(
                                                gameDeckData.DataContext.AddMovieCardToDeck(
                                                    this.deck,
                                                    movie),
                                                movie));
                                    }
                                }

                            }
                        }
                    }

                }

                this.gameCards = cardBuffer.Cards;


                Tracing.TraceInformation(
                    string.Format(
                        CultureInfo.CurrentCulture,
                        Resources.CompletedDeckTraceFormatter,
                        string.Concat(
                            Environment.NewLine, 
                            this.ToString())));
            }
        }

        void PrepareDeck(object state)
        {
            AsynchronousResult typedResult = state as AsynchronousResult;
            if (null == typedResult)
            {
                throw new ManagedException(new ArgumentNullException(GameDeck.StateArgumentName));
            }

            this.DoPrepareDeck();

            typedResult.Complete();
        }

        void Shuffle()
        {
            GameCard[] cardArray = new GameCard[this.gameCards.Count];
            this.gameCards.CopyTo(cardArray, 0);
            byte[] byteArray = new byte[cardArray.Length];
            Game.Randomizer.NextBytes(byteArray);
            Array.Sort(byteArray, cardArray);
            this.gameCards = new ReadOnlyCollection<GameCard>(cardArray);
        } 

        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
            int limit = this.gameCards.Count;
            for(int index = 1; index <= limit; index++)
            {
                result.AppendLine(
                    string.Format(
                        CultureInfo.CurrentCulture,
                        GameDeck.StringFormatter,
                        index,
                        this.gameCards[index - 1].ToString()));
            }
            return result.ToString();
        }

        class CardBuffer
        {
            readonly List<GameCard> cardBuffer;
            readonly Queue<GameCard> cardQueue;

            internal event EventHandler<AddedCardEventArgs> AddedCard;

            internal CardBuffer()
            {
                this.cardBuffer = new List<GameCard>(GameDeck.DeckSize);
                this.cardQueue = new Queue<GameCard>(GameDeck.DeckSize);
            }

            internal ReadOnlyCollection<GameCard> Cards
            {
                get
                {
                    return new ReadOnlyCollection<GameCard>(this.cardBuffer);
                }
            }

            internal int Count
            {
                get
                {
                    return this.cardBuffer.Count;
                }
            }

            internal void AddCard(GameCard card)
            {
                if (this.cardBuffer.Count < GameDeck.DeckSize)
                {
                    this.cardBuffer.Add(card);
                    this.cardQueue.Enqueue(card);

                    if (this.AddedCard != null)
                    {
                        this.AddedCard(this, new AddedCardEventArgs((byte)this.cardBuffer.Count));
                    }
                }
            }

            internal bool TryDequeue(out GameCard card)
            {
                card = null;
                if (this.cardQueue.Count > 0)
                {
                    card = this.cardQueue.Dequeue();
                    return true;
                }
                return false;
            }
        }

        class GameDeckData : IDisposable
        {
            GameData gameData;

            internal GameDeckData()
            {
                this.gameData = new GameData();
            }

            internal data.MoviePokerEntities DataContext
            {
                get
                {
                    return this.gameData.DataContext;
                }
            }
            
            public void Dispose()
            {
                this.gameData.Dispose();
                GC.SuppressFinalize(this);
            }

            data.PersonExternalIdentifier[] RandomlySelectPeople(data.Movie movie, data.Deck deck)
            {
                List<data.PersonExternalIdentifier> result = new List<data.PersonExternalIdentifier>(3);

                if (movie.Populated)
                {
                    for (int index = 1; index <= 2; index++)
                    {
                        result.AddRange(
                            GameDeckData.RandomlySelectIdentifier<data.PersonExternalIdentifier>(
                                1 == index ?
                                    (
                                        from movieItem in this.DataContext.Movie
                                        from personExternalIdentifierItem in movieItem.MovieExternalIdentifier.MovieActorPersonExternalIdentifier
                                        where 
                                                movieItem.MovieKey == movie.MovieKey
                                        select personExternalIdentifierItem
                                    ).ToArray():
                                    (
                                        from movieItem in this.DataContext.Movie
                                        from personExternalIdentifierItem in movieItem.MovieExternalIdentifier.MovieDirectorPersonExternalIdentifier
                                        where 
                                                movieItem.MovieKey == movie.MovieKey
                                        select personExternalIdentifierItem
                                    ).ToArray(),
                                (
                                    (
                                        from resultItem in result
                                        select resultItem
                                    ).Union
                                    (
                                        from personItem in
                                            (
                                                from cardItem in
                                                    (
                                                        from deckItem in this.DataContext.Deck
                                                        from deckCardItem in deckItem.DeckCard
                                                        where 
                                                                deckItem.DeckKey == deck.DeckKey
                                                        select deckCardItem.Card
                                                    )
                                                select cardItem.Person.FirstOrDefault()
                                            )
                                        select 
                                            (personItem != null)?
                                                personItem.PersonExternalIdentifier:
                                                null
                                    )
                                ).ToArray(),
                                Math.Min(index, (GameDeck.DeckSize - deck.DeckCard.Count))));
                    }
                }

                return result.ToArray();
            }

            static T[] RandomlySelectIdentifier<T>(
                T[] identifiers,
                T[] excludedIdentifiers,
                int numberToSelect) where T: class
            {
                List<T> result = new List<T>(numberToSelect);
                int candidateCount;
                while (true)
                {
                    var candidateQuery =
                        from candidateItem in identifiers
                        where
                                (!(excludedIdentifiers.Contains(candidateItem)))
                            && (!(result.Contains(candidateItem)))
                        select candidateItem;

                    candidateCount = candidateQuery.Count();
                    if (candidateCount > 0)
                    {
                        result.Add(candidateQuery.ElementAt(Game.Randomizer.Next(0, candidateCount - 1)));

                    }
                    else
                    {
                        break;
                    }

                    if (result.Count >= numberToSelect)
                    {
                        break;
                    }
                }

                return result.ToArray();
            }

            internal data.Movie RandomlySelectMovie(int difficultyQuotient, data.Deck deck)
            {
                data.Movie result = null;

                for (int attemptIndex = 0; attemptIndex < GameDeck.MovieSelectionAttemptsLimit; attemptIndex++)
                {
                    if (this.DataContext.TryRandomlySelectMovieByDifficulty(difficultyQuotient, deck.DeckKey, out result))
                    {
                        if (this.TryRetrieveMovie(result.MovieExternalIdentifier.ExternalIdentifierValue, out result))
                        {
                            return result;
                        }
                    }
                    else
                    {
                        throw new ManagedException(new GameException(Resources.DeckCreationMovieSelectionException));
                    }
                }

                throw new ManagedException(new GameException(Resources.DeckCreationException));
            }

            

            internal bool TryProcessMovieCard(
                MovieCard movieCard,
                data.Deck deck,
                out data.Person[] people,
                out data.YearTopGrossingMovie yearTopGrossingMovie)
            {
                data.Movie movie = movieCard.Movie;
                bool peopleResult = this.TrySelectPeopleFromMovie(movie, deck, out people);
                bool yearTopGrossingMovieResult = this.TrySelectTopGrossingMovieOfTheYear(movie, deck, out yearTopGrossingMovie);
                return (peopleResult || yearTopGrossingMovieResult);
            }

            internal bool TryProcessPersonCard(
                PersonCard personCard,
                data.Deck deck,
                out data.Movie movie)
            {
                movie = null;

                data.MovieExternalIdentifier movieIdentifier;
                data.Movie movieBuffer = null;
                if
                (
                    (this.TrySelectMovieFromPeople(personCard, deck, out movieIdentifier)) &&
                    (this.TryRetrieveMovie(movieIdentifier.ExternalIdentifierValue, out movieBuffer))
                )
                {
                    movie = movieBuffer;
                    return true;
                }

                return false;
            }

            internal bool TryProcessYearCard(
                YearCard yearCard,
                data.Deck deck,
                out data.Movie movie)
            {
                movie = null;

                var movieQuery =
                    from deckItem in this.DataContext.Deck
                    from deckCardItem in deckItem.DeckCard
                    from movieItem in deckCardItem.Card.Movie
                    from yearCardItem in this.DataContext.Card
                    from yearTopGrossingMovieItem in yearCardItem.YearTopGrossingMovie
                    where
                            deckItem.DeckKey == deck.DeckKey
                        &&  yearCardItem.CardKey == yearCard.Card.CardKey
                        && movieItem.MovieKey == yearTopGrossingMovieItem.MovieKey
                    select movieItem;
                if (movieQuery.Count() <= 0)
                {
                    movie = yearCard.YearTopGrossingMovie.Movie;
                    return true;
                }

                return false;
            }

            bool TryRandomlySelectPeople(data.Movie movie, data.Deck deck, out data.PersonExternalIdentifier[] identifiers)
            {
                identifiers = this.RandomlySelectPeople(movie, deck);
                if (identifiers.Length > 0)
                {
                    return true;
                }
                return false;
            }

            internal bool TryRetrieveMovie(string externalIdentifier, out data.Movie movie)
            {
                return this.gameData.TryRetrieveMovie(externalIdentifier, out movie);
            }

            internal bool TryRetrievePerson(string externalIdentifier, out data.Person person)
            {
                return this.gameData.TryRetrievePerson(externalIdentifier, out person);
            }

            bool TrySelectMovieFromPeople(PersonCard personCard, data.Deck deck, out data.MovieExternalIdentifier identifier)
            {
                
                identifier = null;

                data.Person person = personCard.Person;
                if (null != person)
                {
                    Tracing.TraceInformation(Resources.DeckCreationSelectingMovieFromPersonTrace, deck, person);

                    if (person.Populated)
                    {
                        data.MovieExternalIdentifier[] result =
                            GameDeckData.RandomlySelectIdentifier<data.MovieExternalIdentifier>(
                                (
                                    from personItem in this.DataContext.Person
                                    from movieExternalIdentifierItem in personItem.PersonExternalIdentifier.DirectingCreditMovieExternalIdentifier
                                    select movieExternalIdentifierItem
                                ).Union
                                (
                                    from personItem in this.DataContext.Person
                                    from movieExternalIdentifierItem in personItem.PersonExternalIdentifier.ActingCreditMovieExternalIdentifier
                                    select movieExternalIdentifierItem
                                ).ToArray(),
                                (
                                    from movieItem in
                                        (
                                            from cardItem in
                                                (
                                                    from deckItem in this.DataContext.Deck
                                                    from deckCardItem in deckItem.DeckCard
                                                    where 
                                                            deckItem.DeckKey == deck.DeckKey
                                                    select deckCardItem.Card
                                                )
                                            select cardItem.Movie.FirstOrDefault()
                                        )
                                    select
                                        (movieItem != null) ?
                                            movieItem.MovieExternalIdentifier :
                                            null
                                ).ToArray(),
                                Math.Min(1, (GameDeck.DeckSize - deck.DeckCard.Count)));

                        if (result.Length > 0)
                        {
                            identifier = result[0];
                            return true;
                        }
                    }
                }

                return false;
            }


            bool TrySelectPeopleFromMovie(
                data.Movie movie,
                data.Deck deck,
                out data.Person[] people)
            {
                Tracing.TraceInformation(Resources.DeckCreationSelectingPeopleFromMovieTrace, deck, movie);

                people = null;

                data.PersonExternalIdentifier[] identifiers = null;
                data.Person person = null;
                List<data.Person> peopleBuffer = new List<data.Person>();

                movie.Load();
                if (this.TryRandomlySelectPeople(movie, deck, out identifiers))
                {
                    foreach (data.PersonExternalIdentifier currentIdentifier in identifiers)
                    {
                        if (this.TryRetrievePerson(currentIdentifier.ExternalIdentifierValue, out person))
                        {
                            peopleBuffer.Add(person);
                        }
                    }
                }

                if (peopleBuffer.Count > 0)
                {
                    people = peopleBuffer.ToArray();
                    return true;
                }

                return false;
            }

            bool TrySelectTopGrossingMovieOfTheYear(
                data.Movie movie,
                data.Deck deck,
                out data.YearTopGrossingMovie yearTopGrossingMovie)
            {
                Tracing.TraceInformation(Resources.DeckCreationSelectingTopGrossingMovieOfTheYearFromMovieTrace, deck, movie);

                yearTopGrossingMovie = null;

                data.Movie otherMovie = null;
                short year = movie.Year;

                var yearCardQuery =
                    from deckItem in this.DataContext.Deck
                    from deckCardItem in deckItem.DeckCard
                    from yearTopGrossingMovieItem in deckCardItem.Card.YearTopGrossingMovie
                    where 
                            deckItem.DeckKey == deck.DeckKey
                        &&  yearTopGrossingMovieItem.Movie.Year == year
                    select yearTopGrossingMovieItem;
                if (yearCardQuery.Count() > 0)
                {
                    Tracing.TraceInformation(Resources.DeckContainsTopGrossingMovieOfTheYear, year);
                    return false;
                }

                var yearTopGrossingQuery =
                    from topGrossingItem in this.DataContext.YearTopGrossingMovie
                    join movieItem in this.DataContext.Movie
                        on topGrossingItem.MovieKey equals movieItem.MovieKey
                    where
                            movieItem.Year == year
                    select topGrossingItem;
                yearTopGrossingMovie = yearTopGrossingQuery.FirstOrDefault();
                if (null == yearTopGrossingMovie)
                {
                    Tracing.TraceInformation(Resources.TopGrossingMovieOfTheYearUnknown, year);
                    return false;
                }

                yearTopGrossingMovie.Load();

                if (yearTopGrossingMovie.MovieKey == movie.MovieKey)
                {
                    Tracing.TraceInformation(Resources.CurrentMovieTopGrossingMovieOfTheYear, movie.MovieKey, year);
                    return true;
                }
                else
                {
                    if (this.TryRetrieveMovie(
                        yearTopGrossingMovie.Movie.MovieExternalIdentifier.ExternalIdentifierValue,
                        out otherMovie))
                    {
                        Tracing.TraceInformation(Resources.RetrievedTopGrossingMovieOfTheYear, otherMovie.MovieKey, year);
                        return true;
                    }
                }

                return false;
            }
            
        }
        
    }
}
