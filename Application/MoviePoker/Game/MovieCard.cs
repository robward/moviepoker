﻿namespace MoviePoker
{
    using System.Globalization;
    using MoviePoker.Properties;
    using data = MoviePoker.Data.LocalDataAccess;
    
    class MovieCard: GameCard
    {
        const string StringFormatter = "{0}: {1}";

        readonly data.Movie movie;

        internal MovieCard(data.Card card, data.Movie movie)
            :base(card)
        {
            this.movie = movie;
        }

        internal data.Movie Movie
        {
            get
            {
                return this.movie;
            }
        }

        public override string ToString()
        {
            return string.Format(
                CultureInfo.CurrentCulture, 
                MovieCard.StringFormatter, 
                Resources.MovieCardString, 
                this.movie.Title);
        }
    }
}
