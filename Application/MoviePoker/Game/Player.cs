﻿namespace MoviePoker
{
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Text;
    using MoviePoker.Properties;

    public class Player
    {
        const string StringFormatter = "{0}: {1}";

        readonly string name;
        
        internal Player(string name, byte initialDealingOrderPosition)
        {
            this.name = name;
            this.DealingOrderPosition = initialDealingOrderPosition;
        }

        public string Name
        {
            get
            {
                return this.name;
            }
        }

        public byte DealingOrderPosition
        {
            get;
            set;
        }

        public ReadOnlyCollection<GameCard> Hand
        {
            get;
            internal set;
        }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
            result.AppendLine(
                string.Format(
                    CultureInfo.CurrentCulture,
                    Resources.PlayerFormatter,
                    this.name));
            int limit = this.Hand.Count;
            for (int index = 1; index <= limit; index++)
            {
                result.AppendLine(
                    string.Format(
                        CultureInfo.CurrentCulture,
                        Player.StringFormatter,
                        index,
                        this.Hand[index - 1].ToString()));
            }
            return result.ToString();
        }

    }
}
