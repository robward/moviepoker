﻿namespace MoviePoker
{
    using System.Globalization;
    using MoviePoker.Properties;
    using data = MoviePoker.Data.LocalDataAccess;
    
    class PersonCard: GameCard
    {
        const string StringFormatter = "{0}: {1}";

        readonly data.Person person;

        internal PersonCard(data.Card card, data.Person person)
            :base(card)
        {
            this.person = person;
        }

        internal data.Person Person
        {
            get
            {
                return this.person;
            }
        }

        public override string ToString()
        {
            return string.Format(
                CultureInfo.CurrentCulture,
                PersonCard.StringFormatter,
                Resources.PersonCardString,
                this.person.Name);
        }
    }
}
