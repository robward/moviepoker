﻿namespace MoviePoker
{
    using System.Globalization;
    using MoviePoker.Properties;
    using data = MoviePoker.Data.LocalDataAccess;
    
    class YearCard: GameCard
    {
        const string StringFormatter = "{0}: {1} ({2})";

        data.YearTopGrossingMovie yearTopGrossingMovie;

        internal YearCard(data.Card card, data.YearTopGrossingMovie movie)
            :base(card)
        {
            this.yearTopGrossingMovie = movie;
        }

        internal data.YearTopGrossingMovie YearTopGrossingMovie
        {
            get
            {
                return this.yearTopGrossingMovie;
            }
        }

        public override string ToString()
        {
            this.yearTopGrossingMovie.Load();
            return string.Format(
                CultureInfo.CurrentCulture,
                YearCard.StringFormatter,
                Resources.YearCardString,
                this.yearTopGrossingMovie.Movie.Year,
                this.yearTopGrossingMovie.Movie.Title);
        }
    }
}
