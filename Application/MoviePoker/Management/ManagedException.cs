﻿namespace MoviePoker.Management
{
    using System;
    using System.Globalization;
    using System.Runtime.Serialization;
    using MoviePoker.Management.Properties;
   
    [Serializable]
    public class ManagedException: Exception
    {
        const string MessageExceptionFormatter = "{0} {1}";

        public ManagedException()
            :this(Resources.UnspecifiedExceptionMessage)
        {

        }

        public ManagedException(string message)
            :base(message)
        {
            Tracing.TraceError(message);
        }

        public ManagedException(Exception cause)
            :base(cause.Message, cause.InnerException)
        {
            Tracing.TraceError(cause);
        }

        protected ManagedException(SerializationInfo serializationInformation, StreamingContext streamingContext)
            :base(serializationInformation, streamingContext)
        {
        }

        public ManagedException(string message, Exception innerException)
            : base(message, innerException)
        {
            Tracing.TraceError(
                string.Format(
                    CultureInfo.InvariantCulture,
                    ManagedException.MessageExceptionFormatter,
                    message, 
                    innerException));
        }
    }
}
