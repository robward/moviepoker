﻿namespace MoviePoker.Management
{
    using System;
    using System.Diagnostics;
    using System.Globalization;
    using MoviePoker.Management.Properties;
    
    public static class Tracing
    {
        const string DefaultTextTraceListenerOutputFileName = "MoviePoker.log";

        const int TraceLengthLimit = 32765;

        static Tracing()
        {
            EventTypeFilter defaultTraceFilter = new EventTypeFilter(
#if DEBUG
                SourceLevels.Verbose
#else
                SourceLevels.Information
#endif
                    );
            
            TraceListener defaultListener = null;
            for (byte index = 0; index < 2; index++)
            {
                switch (index)
                {
                    case 0:
                        defaultListener = new EventLogTraceListener(Resources.TraceSource);
                        break;
                    case 1:
                        defaultListener = new TextWriterTraceListener(Tracing.DefaultTextTraceListenerOutputFileName);
                        break;
                }
                defaultListener.Filter = new EventTypeFilter(SourceLevels.Information);
                try
                {
                    Trace.TraceInformation(Resources.TracingInitiatedMessage);
                }
                catch (Exception exception)
                {
                    if (ExceptionAnalyzer.IsFatal(exception))
                    {
                        throw;
                    }
                    Trace.Listeners.Remove(defaultListener);
                    continue;
                }

                defaultListener.Filter = defaultTraceFilter;
                break;
            }
        }

        public static void TraceError(Exception exception)
        {
            Tracing.TraceError(exception.ToString());
        }

        public static void TraceError(string error)
        {
            Trace.TraceError(error);
        }

        public static void TraceInformation(string message)
        {
            try
            {
                int messageLength = message.Length;
                Trace.TraceInformation(
                    messageLength > Tracing.TraceLengthLimit ?
                        message.Substring(0, Tracing.TraceLengthLimit) :
                        message);
            }
            catch (Exception exception)
            {
                if (ExceptionAnalyzer.IsFatal(exception))
                {
                    throw;
                }
            }
        }

        public static void TraceInformation(string message, params object[] arguments)
        {
            Tracing.TraceInformation(
                string.Format(
                    CultureInfo.CurrentCulture,
                    message,
                    arguments));
        }
    }
}
