﻿namespace MoviePoker.Management
{
    using System;
    using System.Threading;

    public static class ExceptionAnalyzer
    {
        public static bool IsFatal(Exception exception)
        {
            return 
                exception is ThreadAbortException ||
                exception is ThreadInterruptedException ||
                exception is StackOverflowException ||
                exception is OutOfMemoryException ||
                exception is ExecutionEngineException;
        }
    }
}
