﻿namespace MoviePoker.Management
{
    using System;
    using System.Globalization;
    using System.Runtime.Serialization;
    using MoviePoker.Management.Properties;
   
    [Serializable]
    public class BadIdentifierException: ManagedException
    {
        public BadIdentifierException()
            : base(Resources.DefaultBadIdentifierExceptionMessage)
        {
        }

        public BadIdentifierException(string externalIdentifierValue)
            :base(string.Format(CultureInfo.CurrentCulture, Resources.BadIdentifierExceptionMessage,externalIdentifierValue))
        {
        }

        public BadIdentifierException(Exception cause)
            :base(cause.Message, cause.InnerException)
        {
            Tracing.TraceError(cause);
        }

        protected BadIdentifierException(SerializationInfo serializationInformation, StreamingContext streamingContext)
            :base(serializationInformation, streamingContext)
        {
        }

        public BadIdentifierException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
