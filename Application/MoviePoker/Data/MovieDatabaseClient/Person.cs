﻿namespace MoviePoker.Data.MovieDatabaseClient
{
    using System.Collections.ObjectModel;

    public class Person
    {
        readonly string identifier;
        readonly Collection<string> actingCreditMovieIdentifiers;
        readonly Collection<string> directingCreditMovieIdentifiers;
        readonly string name;

        Person()
        {
        }

        internal Person(
            string identifier, 
            string name, 
            string[] actingCreditMovieIdentifiers,
            string[] directingCreditMovieIdentifiers)
        {
            this.identifier = identifier;
            this.name = name;
            this.actingCreditMovieIdentifiers = new Collection<string>(actingCreditMovieIdentifiers);
            this.directingCreditMovieIdentifiers = new Collection<string>(directingCreditMovieIdentifiers);
        }

        public string Identifier
        {
            get
            {
                return this.identifier;
            }
        }

        public Collection<string> ActingCreditMovieIdentifiers
        {
            get
            {
                return this.actingCreditMovieIdentifiers;
            }
        }

        public Collection<string> DirectingCreditMovieIdentifiers
        {
            get
            {
                return this.directingCreditMovieIdentifiers;
            }
        }

        public string Name
        {
            get
            {
                return this.name;
            }
        }
    }
}
