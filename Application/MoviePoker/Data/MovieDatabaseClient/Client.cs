﻿namespace MoviePoker.Data.MovieDatabaseClient
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.ServiceModel;
    using System.Threading;
    using System.Xml.Linq;
    using MoviePoker.Management;
    using MoviePoker.Utility;
    using MoviePoker.Workflow;

    public sealed class Client : IMovieDatabaseClient, ISharedServiceClient
    {
        delegate Movie MovieDelegate(string identifier);
        delegate MovieSearchResult[] MovieSearchDelegate(string title);
        delegate Person PersonDelegate(string identifier);

        const string CastElementName = "cast";
        const string FilmographyElementName = "filmography";
        const string IdentifierElementName = "id";
        const string JobAttributeName = "job";
        const string MovieElementName = "movie";
        const string NameElementName = "name";
        const string PersonElementName = "person";
        const string ReleaseDateElementName = "released";
        const string RevenueElementName = "revenue";

        const string ActorJobName = "Actor";
        const string DirectorJobName = "Director";

        const string IdentifierArgumentName = "identifier";
        const string ResultArgumentName = "result";
        const string TitleArgumentName = "title";

        const string Space = " ";
        const string EscapedSpace = "%20";

        readonly string address;
        readonly string userKey;

        Dictionary<string, Invocation> invocations;
        SharedServiceClientWorkflowHost workflowHost;

        Client()
        {
            this.invocations = new Dictionary<string, Invocation>();
            this.workflowHost = new SharedServiceClientWorkflowHost(this);
        }

        internal Client(string address, string userKey)
            :this()
        {
            this.address = address;
            this.userKey = userKey;
        }

        public IAsyncResult BeginFindMostSuccessfulMovieByTitle(string title, AsyncCallback callback)
        {
            AsynchronousResult result = new AsynchronousResult(callback, title);
            ThreadPool.QueueUserWorkItem(new WaitCallback(this.FindMostSuccessfulMovieByTitle), result); 
            return result;
        }

        public Movie EndFindMostSuccessfulMovieByTitle(IAsyncResult result)
        {
            AsynchronousResult typedResult = result as AsynchronousResult;
            if (null == typedResult)
            {
                throw new ManagedException(new ArgumentNullException(Client.ResultArgumentName));
            }

            return typedResult.EndInvoke() as Movie;
        }

        public IAsyncResult BeginFindMovieByTitle(string title, AsyncCallback callback)
        {
            return this.EnqueueInvocation(
                new Client.MovieSearchDelegate(this.FindMovieByTitle), 
                callback, 
                new string[] { title });
        }

        public MovieSearchResult[] EndFindMovieByTitle(IAsyncResult result)
        {
            AsynchronousResult typedResult = result as AsynchronousResult;
            if (null == typedResult)
            {
                throw new ManagedException(new ArgumentNullException(Client.ResultArgumentName));
            }

            return typedResult.EndInvoke() as MovieSearchResult[];
        }

        public IAsyncResult BeginGetPerson(string identifier, AsyncCallback callback)
        {
            return this.EnqueueInvocation(
                new Client.PersonDelegate(this.GetPerson),
                callback,
                new string[] { identifier });
        }

        public Person EndGetPerson(IAsyncResult result)
        {
            AsynchronousResult typedResult = result as AsynchronousResult;
            if (null == typedResult)
            {
                throw new ManagedException(new ArgumentNullException(Client.ResultArgumentName));
            }

           return typedResult.EndInvoke() as Person;
        }

        public IAsyncResult BeginGetMovie(string identifier, AsyncCallback callback)
        {
            return this.EnqueueInvocation(
                new Client.MovieDelegate(this.GetMovie),
                callback,
                new string[] { identifier });
        }

        public Movie EndGetMovie(IAsyncResult result)
        {
            AsynchronousResult typedResult = result as AsynchronousResult;
            if (null == typedResult)
            {
                throw new ManagedException(new ArgumentNullException(Client.ResultArgumentName));
            }

            return typedResult.EndInvoke() as Movie;
        }

        public void Dispose()
        {
            this.workflowHost.Dispose();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        public bool DoInvocation(string invocationIdentifier)
        {
            Invocation invocation;
            if (!(this.invocations.TryGetValue(invocationIdentifier, out invocation)))
            {
                return true;
            }

            try
            {
                object result = invocation.Method.Method.Invoke(this, (object[])invocation.Result.AsyncState);
                invocation.Result.Complete(result);
            }
            catch (ServerTooBusyException)
            {
                return false;
            }
            catch (Exception exception)
            {
                invocation.Result.Complete(exception);
            }

            return true;
        }

        AsynchronousResult EnqueueInvocation(Delegate method, AsyncCallback callback, object input)
        {
            AsynchronousResult result = new AsynchronousResult(callback, input);
            string invocationIdentifier = Guid.NewGuid().ToString();
            this.invocations.Add(invocationIdentifier, new Invocation(method, result));
            this.workflowHost.QueueInvocation(invocationIdentifier);
            return result;
        }

        void FindMostSuccessfulMovieByTitle(object result)
        {
            AsynchronousResult typedResult = null;
            try
            {
                typedResult = result as AsynchronousResult;
                if (null == result)
                {
                    throw new ArgumentNullException(Client.ResultArgumentName);
                }
                this.FindMostSuccessfulMovieByTitle(typedResult);
            }
            catch (Exception exception)
            {
                Tracing.TraceError(exception);
                if (ExceptionAnalyzer.IsFatal(exception))
                {
                    throw;
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2208:InstantiateArgumentExceptionsCorrectly")]
        void FindMostSuccessfulMovieByTitle(AsynchronousResult result)
        {
            if (null == result)
            {
                return;
            }

            try
            {
                string title = result.AsyncState as string;
                MovieSearchResult[] moviesByTitle = this.EndFindMovieByTitle(this.BeginFindMovieByTitle(title, null));

                int count = moviesByTitle.Length;

                if (count <= 0)
                {
                    throw new ArgumentException(
                        string.Format(
                            CultureInfo.CurrentCulture,
                            MovieDatabaseClientResources.InvalidMovieTitleError,
                            title),
                        Client.TitleArgumentName);
                }

                if (count < 2)
                {
                    result.Complete(moviesByTitle[0].Identifier);
                    return;
                }

                List<Movie> identifiedMovies = new List<Movie>(count);
                for (int index = 0; index < count; index++)
                {
                    try
                    {
                        identifiedMovies.Add(this.EndGetMovie(this.BeginGetMovie(moviesByTitle[index].Identifier, null)));
                    }
                    catch (ArgumentException)
                    {
                    }
                    catch (NullReferenceException)
                    {
                    }
                }

                var query =
                    from item in identifiedMovies
                    where item.Revenue.HasValue
                    orderby item.Revenue descending
                    select item;
                result.Complete(query.First());
            }
            catch (Exception exception)
            {
                Tracing.TraceError(
                    string.Format(
                        CultureInfo.CurrentCulture,
                        MovieDatabaseClientResources.MovieSearchError,
                        exception.ToString()));
                throw;
            }
        }


        MovieSearchResult[] FindMovieByTitle(string title)
        {
            try
            {
                if (string.IsNullOrEmpty(title))
                {
                    throw new ArgumentNullException(Client.TitleArgumentName);
                }

                XElement movieData = new ClientImplementation(this.address).MovieSearch(
                        this.userKey,
                        title.Replace(Client.Space, Client.EscapedSpace));
                Tracing.TraceInformation(
                    MovieDatabaseClientResources.MovieSearchTrace,
                    title,
                    Environment.NewLine,
                    movieData);

                var query =
                    from item in movieData.Descendants(Client.MovieElementName)
                    where
                            item.Descendants(Client.IdentifierElementName).Count() > 0
                        &&  item.Descendants(Client.ReleaseDateElementName).Count() > 0
                        &&  item.Descendants(Client.ReleaseDateElementName).Single().Value.Length > 0
                    select
                        new MovieSearchResult(
                            (string)item.Element(Client.IdentifierElementName),
                            (string)item.Element(Client.ReleaseDateElementName));
                return query.ToArray();
            }
            catch (Exception exception)
            {
                Tracing.TraceError(
                    string.Format(
                        CultureInfo.CurrentCulture,
                        MovieDatabaseClientResources.MovieSearchError,
                        exception.ToString()));
                throw;
            }
        }

        Movie GetMovie(string identifier)
        {
            try
            {
                XElement movieData = new ClientImplementation(this.address).MovieGet(
                        this.userKey,
                        identifier);
                Tracing.TraceInformation(
                    MovieDatabaseClientResources.MovieRetrievalTrace,
                    identifier,
                    Environment.NewLine,
                    movieData);

                var query =
                    from item in movieData.Descendants(Client.MovieElementName)
                        where
                                item.Descendants(Client.IdentifierElementName).Count() > 0
                    select
                        new Movie(
                            identifier,
                            (string)item.Element(Client.NameElementName),
                            (string)item.Element(Client.ReleaseDateElementName),
                            (string)item.Element(Client.RevenueElementName),
                            (
                                from personElement in item.Element(
                                        Client.CastElementName).Descendants(Client.PersonElementName)
                                where 
                                        personElement.Attribute(Client.JobAttributeName).Value.Equals(Client.DirectorJobName,StringComparison.OrdinalIgnoreCase)
                                select personElement.Attribute(Client.IdentifierElementName).Value
                            ).ToArray(),
                            (
                                from personElement in item.Element(Client.CastElementName).Descendants(Client.PersonElementName)
                                where 
                                        personElement.Attribute(Client.JobAttributeName).Value.Equals(Client.ActorJobName, StringComparison.OrdinalIgnoreCase)
                                select personElement.Attribute(Client.IdentifierElementName).Value
                            ).ToArray());

                if (query.Count() != 1)
                {
                    throw new ArgumentException(
                        string.Format(
                            CultureInfo.CurrentCulture,
                            MovieDatabaseClientResources.InvalidMovieIdentifierError,
                            identifier),
                        Client.IdentifierArgumentName);
                }
                return query.Single();
            }
            catch (Exception exception)
            {
                Tracing.TraceError(
                    string.Format(
                        CultureInfo.CurrentCulture,
                        MovieDatabaseClientResources.MovieSearchError,
                        exception.ToString()));
                throw;
            }
        }

        Person GetPerson(string identifier)
        {
            try
            {
                XElement personData = new ClientImplementation(this.address).PersonGet(
                       this.userKey,
                       identifier);
                Tracing.TraceInformation(
                    MovieDatabaseClientResources.PersonRetrievalTrace,
                    identifier,
                    Environment.NewLine,
                    personData);

                var query =
                   from item in personData.Descendants(Client.PersonElementName)
                    where
                            item.Descendants(Client.IdentifierElementName).Count() > 0
                   select
                       new Person
                           (
                               identifier,
                               (string)item.Element(Client.NameElementName),
                               (
                                   from movieElement in item.Element(
                                       Client.FilmographyElementName).Descendants(Client.MovieElementName)
                                   where movieElement.Attribute(Client.JobAttributeName).Value.Equals(Client.ActorJobName, StringComparison.OrdinalIgnoreCase)
                                   select movieElement.Attribute(Client.IdentifierElementName).Value
                               ).ToArray(),
                               (
                                   from movieElement in item.Element(
                                       Client.FilmographyElementName).Descendants(Client.MovieElementName)
                                   where movieElement.Attribute(Client.JobAttributeName).Value.Equals(Client.DirectorJobName, StringComparison.OrdinalIgnoreCase)
                                   select movieElement.Attribute(Client.IdentifierElementName).Value
                               ).ToArray()
                            );

                if (query.Count() != 1)
                {
                    throw new ArgumentException(
                        string.Format(
                            CultureInfo.CurrentCulture,
                            MovieDatabaseClientResources.InvalidPersonIdentifierError,
                            identifier),
                        Client.IdentifierArgumentName);
                }
                return query.Single();
            }
            catch (Exception exception)
            {
                Tracing.TraceError(
                    string.Format(
                        CultureInfo.CurrentCulture,
                        MovieDatabaseClientResources.MovieSearchError,
                        exception.ToString()));
                throw;
            }
        }

        class Invocation
        {
            readonly Delegate method;
            readonly AsynchronousResult result;

            Invocation()
            {
            }

            internal Invocation(Delegate method, AsynchronousResult data)
            {
                this.method = method;
                this.result = data;
            }

            internal Delegate Method
            {
                get
                {
                    return this.method;
                }
            }

            internal AsynchronousResult Result
            {
                get
                {
                    return this.result;
                }
            }
        }
    }
}
