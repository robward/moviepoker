﻿namespace MoviePoker.Data.MovieDatabaseClient
{
    using System;
    using System.Globalization;

    public class MovieSearchResult
    {
        readonly string identifier;
        readonly DateTime? releaseDate;

        MovieSearchResult()
        {
        }

        internal MovieSearchResult(string identifier, string releaseDate)
        {
            this.identifier = identifier;

            DateTime parsedDate = DateTime.Now;
            if (DateTime.TryParse(
                releaseDate, 
                CultureInfo.InvariantCulture, 
                DateTimeStyles.AssumeUniversal, 
                out parsedDate))
            {
                this.releaseDate = parsedDate;
            }
        }

        public string Identifier
        {
            get
            {
                return this.identifier;
            }
        }

        public DateTime? ReleaseDate
        {
            get
            {
                return this.releaseDate;
            }
        }
    }
}
