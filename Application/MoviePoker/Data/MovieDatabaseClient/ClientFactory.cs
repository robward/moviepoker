﻿namespace MoviePoker.Data.MovieDatabaseClient
{
    public static class ClientFactory
    {
        public static IMovieDatabaseClient Create(string address, string userKey)
        {
            return new Client(address, userKey);
        }
    }
}
