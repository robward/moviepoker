﻿namespace MoviePoker.Data.MovieDatabaseClient
{
    using System;
    using System.Collections.ObjectModel;
    using System.Globalization;

    public class Movie
    {
        readonly Collection<string> actorIdentifiers;
        readonly Collection<string> directorIdentifiers;
        readonly string identifier;
        readonly long? revenue;
        readonly DateTime? releaseDate;
        readonly string title;

        Movie()
        {
        }

        internal Movie(
            string identifier, 
            string title,
            string releaseDate,
            string revenue,
            string[] directorIdentifiers,
            string[] actorIdentifiers)
        {
            this.identifier = identifier;
            this.title = title;

            DateTime parsedDate = DateTime.Now;
            if (DateTime.TryParse(
                releaseDate, 
                CultureInfo.InvariantCulture, 
                DateTimeStyles.AssumeUniversal|DateTimeStyles.AdjustToUniversal, //Takes a date with no time or timezone specified and returns that date at midnight, with no timezone specified.
                out parsedDate))
            {
                this.releaseDate = parsedDate;
            }

            long parsedRevenue = 0;
            if(long.TryParse(
                revenue,
                NumberStyles.Integer,
                CultureInfo.InvariantCulture,
                out parsedRevenue))
            {
                this.revenue = parsedRevenue;
            }

            this.directorIdentifiers = new Collection<string>(directorIdentifiers);
            this.actorIdentifiers = new Collection<string>(actorIdentifiers);
            
        }

        public Collection<string> ActorIdentifiers
        {
            get
            {
                return this.actorIdentifiers;
            }
        }

        public Collection<string> DirectorIdentifiers
        {
            get
            {
                return this.directorIdentifiers;
            }
        }

        public string Identifier
        {
            get
            {
                return this.identifier;
            }
        }

        public DateTime? ReleaseDate
        {
            get
            {
                return this.releaseDate;
            }
        }

        public long? Revenue
        {
            get
            {
                return this.revenue;
            }
        }

        public string Title
        {
            get
            {
                return this.title;
            }
        }
    }
}
