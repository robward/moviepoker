﻿namespace MoviePoker.Data.MovieDatabaseClient
{
    using System.ServiceModel;
    using System.ServiceModel.Web;
    using System.Xml.Linq;

    [ServiceContract]
    interface IMovieDatabase
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2243:AttributeStringLiteralsShouldParseCorrectly"), OperationContract]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Xml,
            UriTemplate = "Movie.getInfo/en/xml/{userKey}/{identifier}")]
        XElement MovieGet(string userKey, string identifier);

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2243:AttributeStringLiteralsShouldParseCorrectly"), OperationContract]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat=WebMessageFormat.Json,
            ResponseFormat=WebMessageFormat.Xml,
            UriTemplate = "Movie.search/en/xml/{userKey}/{title}")]
        XElement MovieSearch(string userKey, string title);

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2243:AttributeStringLiteralsShouldParseCorrectly"), OperationContract]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Xml,
            UriTemplate = "Person.getInfo/en/xml/{userKey}/{identifier}")]
        XElement PersonGet(string userKey, string identifier);
    }
}
