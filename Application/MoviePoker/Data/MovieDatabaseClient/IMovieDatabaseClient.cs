﻿namespace MoviePoker.Data.MovieDatabaseClient
{
    using System;

    public interface IMovieDatabaseClient: IDisposable
    {
        IAsyncResult BeginFindMostSuccessfulMovieByTitle(string title, AsyncCallback callback);
        Movie EndFindMostSuccessfulMovieByTitle(IAsyncResult result);
        IAsyncResult BeginFindMovieByTitle(string title, AsyncCallback callback);
        MovieSearchResult[] EndFindMovieByTitle(IAsyncResult result);
        IAsyncResult BeginGetPerson(string identifier, AsyncCallback callback);
        Person EndGetPerson(IAsyncResult result);
        IAsyncResult BeginGetMovie(string identifier, AsyncCallback callback);
        Movie EndGetMovie(IAsyncResult result);
    }
}
