﻿namespace MoviePoker.Data.MovieDatabaseClient
{
    using System.Configuration;

    public class MovieDatabaseClientSection: ConfigurationSection
    {
        public const string SectionName = "movieDatabase";
        const string AddressElementName = "address";
        const string UserKeyElementName = "userKey";

        [ConfigurationProperty(MovieDatabaseClientSection.AddressElementName, IsRequired=true)]
        public string Address
        {
            get
            {
                return (string)this[MovieDatabaseClientSection.AddressElementName];
            }

            set
            {
                this[MovieDatabaseClientSection.AddressElementName] = value;
            }
        }

        [ConfigurationProperty(MovieDatabaseClientSection.UserKeyElementName, IsRequired=true)]
        public string UserKey
        {
            get
            {
                return (string)this[MovieDatabaseClientSection.UserKeyElementName];
            }

            set
            {
                this[MovieDatabaseClientSection.UserKeyElementName] = value;
            }
        }
    }
}
