﻿namespace MoviePoker.Data.MovieDatabaseClient
{
    using System.ServiceModel;
    using System.ServiceModel.Description;
    using System.Xml.Linq;

    class ClientImplementation: ClientBase<IMovieDatabase>, IMovieDatabase
    {
        ClientImplementation()
        {
        }

        internal ClientImplementation(string endpointAddress) :
            base(new WebHttpBinding(WebHttpSecurityMode.None), new EndpointAddress(endpointAddress))
        {
            base.Endpoint.Behaviors.Add(new WebHttpBehavior());
        }

        public XElement MovieGet(string userKey, string identifier)
        {
            return base.Channel.MovieGet(userKey, identifier);
        }

        public XElement MovieSearch(string userKey, string title)
        {
            return base.Channel.MovieSearch(userKey, title); 
        }

        public XElement PersonGet(string userKey, string identifier)
        {
            return base.Channel.PersonGet(userKey, identifier);
        }
    }
}
