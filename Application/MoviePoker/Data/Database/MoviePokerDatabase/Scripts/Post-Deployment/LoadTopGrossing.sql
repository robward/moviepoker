DECLARE 
    @trueFlag BIT,
    @falseFlag BIT;
    
SELECT 
    @trueFlag = CONVERT(BIT,1),
    @falseFlag = CONVERT(BIT,0);


DECLARE @buffer TABLE
(
	[Title]	NVARCHAR(400),
	[TheMovieDBIdentifier] VARCHAR(16),
	[WorldwideRevenue] BIGINT,
	[Year] SMALLINT,
	[TopGrossing] BIT,
	[TopGrossOfYear] BIT
);

INSERT into @buffer
(
	[Title],
	[TheMovieDBIdentifier],
	[WorldwideRevenue],
	[Year],
	[TopGrossing],
	[TopGrossOfYear]
)
VALUES
('101 Dalmations','12230',38562000,1961,@falseFlag,@trueFlag),
('300','1271',422610419,2007,@trueFlag,@falseFlag),
('Aladdin','812',504050219,1992,@trueFlag,@trueFlag),
('Alvin and the Chipmunks','6477',353435370,2007,@trueFlag,@falseFlag),
('Armageddon','95',553709783,1998,@trueFlag,@falseFlag),
('Austin Powers in Goldmember','818',296633907,2002,@trueFlag,@falseFlag),
('Austin Powers: The Spy Who Shagged Me','817',310940086,1999,@trueFlag,@falseFlag),
('Back to the Future','105',381109762,1985,@trueFlag,@trueFlag),
('Bambi','3170',103037700,1942,@falseFlag,@trueFlag),
('Batman','268',413200000,1989,@trueFlag,@trueFlag),
('Batman Begins','272',371853783,2005,@trueFlag,@falseFlag),
('Ben-Hur','665',36992088,1959,@falseFlag,@trueFlag),
('Beverly Hills Cop','90',316360478,1984,@trueFlag,@falseFlag),
('Bridge on the River Kwai','1026',17195000,1957,@falseFlag,@trueFlag),
('Bruce Almighty','310',484572835,2003,@trueFlag,@falseFlag),
('Butch Cassidy and the Sundance Kid','642',102308889,1969,@trueFlag,@trueFlag),
('Cars','920',461979982,2006,@trueFlag,@falseFlag),
('Cast Away','8358',429632142,2000,@trueFlag,@falseFlag),
('Charlie and the Chocolate Factory','118',473000000,2005,@trueFlag,@falseFlag),
('Cinderella','11224',89569660,1950,@trueFlag,@trueFlag),
('Close Encounters of the Third Kind','840',303788635,1977,@trueFlag,@falseFlag),
('Doctor Zhivago','907',111721910,1965,@trueFlag,@trueFlag),
('E.T. the Extra-Terrestrial','601',792910554,1982,@trueFlag,@trueFlag),
('Fantasia','756',76000000,1940,@trueFlag,@trueFlag),
('Fiddler on the Roof','14811',38251196,1971,@falseFlag,@trueFlag),
('Finding Nemo','12',864625978,2003,@trueFlag,@falseFlag),
('Forrest Gump','13',679400000,1994,@trueFlag,@trueFlag),
('Ghost','251',505000000,1990,@trueFlag,@falseFlag),
('Ghostbusters','620',291632124,1984,@trueFlag,@trueFlag),
('Goldfinger','658',124900000,1964,@trueFlag,@trueFlag),
('Gone with the Wind','770',400176459,1939,@trueFlag,@trueFlag),
('The Graduate','730',44090729,1967,@falseFlag,@trueFlag),
('Grease','621',96300000,1978,@falseFlag,@trueFlag),
('Hancock','8960',624029371,2008,@trueFlag,@falseFlag),
('Happy Feet','9836',384334303,2006,@trueFlag,@falseFlag),
('Harry Potter and the Chamber of Secrets','672',876688482,2002,@trueFlag,@falseFlag),
('Harry Potter and the Goblet of Fire','674',892213036,2005,@trueFlag,@falseFlag),
('Harry Potter and the Prisoner of Azkaban','673',789804554,2004,@trueFlag,@falseFlag),
('Harry Potter and the Sorcerer''s Stone','671',976475550,2001,@trueFlag,@trueFlag),
('Hawaii','14689',15553018,1966,@falseFlag,@trueFlag),
('Home Alone','771',477561243,1990,@trueFlag,@trueFlag),
('How the Grinch Stole Christmas','8871',345141403,2000,@falseFlag,@trueFlag),
('How the West was Won','11897',20932883,1962,@falseFlag,@trueFlag),
('I Am Legend','6479',583184161,2007,@trueFlag,@falseFlag),
('Ice Age: The Meltdown','950',647260749,2002,@trueFlag,@falseFlag),
('Independence Day','602',816969268,1996,@trueFlag,@trueFlag),
('Indiana Jones and the Kingdom of the Crystal Skull','217',520141229,2008,@trueFlag,@falseFlag),
('Indiana Jones and the Last Crusade','89',474000000,1989,@trueFlag,@falseFlag),
('Indiana Jones and the Temple of Doom','87',333000000,1984,@trueFlag,@falseFlag),
('Iron Man','1726',460028823,2008,@trueFlag,@falseFlag),
('Jaws','578',470653000,1975,@trueFlag,@trueFlag),
('Jurassic Park','329',920100000,1993,@trueFlag,@trueFlag),
('King Kong','254',550000000,2005,@trueFlag,@trueFlag),
('Kramer vs. Kramer','12102',59986335,1979,@falseFlag,@trueFlag),
('Kung Fu Panda','9502',631619244,2008,@trueFlag,@falseFlag),
('Lady and the Tramp','10340',40249000,1955,@falseFlag,@trueFlag),
('Lawrence of Arabia','947',44824144,1962,@trueFlag,@trueFlag),
('Love Story','20493',5000000,1970,@trueFlag,@trueFlag),
('Mary Poppins','433',102272727,1964,@trueFlag,@falseFlag),
('Meet the Fockers','693',516533043,2004,@trueFlag,@falseFlag),
('Men in Black','607',589390539,1997,@trueFlag,@falseFlag),
('Mission: Impossible II','955',285176741,2000,@trueFlag,@falseFlag),
('Monsters, Inc.','585',524000000,2001,@trueFlag,@falseFlag),
('Mrs. Doubtfire','788',441195243,1993,@trueFlag,@falseFlag),
('My Big Fat Greek Wedding','8346',368744044,2002,@trueFlag,@falseFlag),
('National Lampoon''s Animal House','8469',141000000,1978,@trueFlag,@trueFlag),
('National Treasure: Book of Secrets','6637',457363168,2007,@trueFlag,@falseFlag),
('Night at the Museum','1593',549736156,2006,@trueFlag,@falseFlag),
('One Flew Over the Cuckoo''s Nest','510',108981275,1975,@trueFlag,@falseFlag),
('Pearl Harbor','676',449220945,2001,@trueFlag,@falseFlag),
('Peter Pan','10693',24532000,1953,@falseFlag,@trueFlag),
('Pinocchio','10895',71846260,1940,@falseFlag,@trueFlag),
('Pirates of the Caribbean: At World''s End','285',961000000,2007,@trueFlag,@falseFlag),
('Pirates of the Caribbean: Dead Man''s Chest','58',1065659812,2006,@trueFlag,@trueFlag),
('Pirates of the Caribbean: The Curse of the Black Pearl','22',655011224,2003,@trueFlag,@falseFlag),
('Quo Vadis?','11620',11901662,1951,@falseFlag,@trueFlag),
('Raiders of the Lost Ark','85',383000000,1981,@trueFlag,@trueFlag),
('Rain Man','380',354825435,1987,@falseFlag,@trueFlag),
('Ratatouille','2062',621325849,2007,@trueFlag,@falseFlag),
('Rocky','1374',300473716,1976,@trueFlag,@trueFlag),
('Rush Hour 2','5175',347325802,2001,@trueFlag,@falseFlag),
('Saving Private Ryan','857',481840909,1998,@trueFlag,@trueFlag),
('Shrek','808',484409218,2001,@trueFlag,@falseFlag),
('Shrek 2','809',920665658,2004,@trueFlag,@trueFlag),
('Shrek the Third','810',798900000,2007,@trueFlag,@falseFlag),
('Signs','2675',408247917,2002,@trueFlag,@falseFlag),
('Snow White and the Seven Dwarfs','408',8000000,1937,@trueFlag,@trueFlag),
('Spider-Man','557',806742000,2002,@trueFlag,@trueFlag),
('Spider-Man 2','558',783766341,2004,@trueFlag,@falseFlag),
('Spider-Man 3','559',806742000,2007,@trueFlag,@trueFlag),
('Star Trek','13475',337434109,2009,@trueFlag,@falseFlag),
('Star Wars Episode I: The Phantom Menace','1893',924317558,1999,@trueFlag,@trueFlag),
('Star Wars Episode II: Attack of the Clones','1894',649398328,2002,@trueFlag,@falseFlag),
('Star Wars Episode III: Revenge of the Sith','1895',850000000,2005,@trueFlag,@trueFlag),
('Star Wars Episode IV: A New Hope','11',775398007,1977,@trueFlag,@falseFlag),
('Star Wars Episode V: The Empire Strikes Back','1891',538400000,1980,@trueFlag,@falseFlag),
('Star Wars Episode VI: Return of the Jedi','1892',572700000,1983,@trueFlag,@trueFlag),
('Superman','1924',391081192,1978,@trueFlag,@falseFlag),
('Superman Returns','1452',391081192,2006,@trueFlag,@falseFlag),
('Terminator 2: Judgment Day','280',520000000,1991,@trueFlag,@trueFlag),
('The Chronicles of Narnia','411',748806957,2005,@trueFlag,@falseFlag),
('The Da Vinci Code','591',755724413,2006,@trueFlag,@falseFlag),
('The Dark Knight','155',1001921825,2008,@trueFlag,@trueFlag),
('The Godfather','238',245066411,1972,@trueFlag,@trueFlag),
('The Hangover','18785',164717015,2009,@trueFlag,@falseFlag),
('The Lord of the Rings: The Fellowship of the Ring','120',871368364,2001,@trueFlag,@falseFlag),
('The Lord of the Rings: The Return of the King','122',1118888979,2003,@trueFlag,@trueFlag),
('The Lord of the Rings: The Two Towers','121',926287400,2002,@trueFlag,@falseFlag),
('The Lost World: Jurassic Park','330',786686679,1997,@trueFlag,@falseFlag),
('The Matrix Reloaded','604',738599701,2003,@trueFlag,@falseFlag),
('The Sixth Sense','745',672806292,1999,@trueFlag,@falseFlag),
('The Ten Commandments','6844',65000000,1956,@trueFlag,@trueFlag),
('Top Gun','744',79400000,1986,@falseFlag,@trueFlag),
('The Towering Inferno','5919',116000000,1974,@trueFlag,@trueFlag),
('Three Men and a Baby','12154',81313000,1987,@falseFlag,@trueFlag),
('Thunderball','660',141195658,1965,@trueFlag,@trueFlag),
('Titanic','597',1845034188,1997,@trueFlag,@trueFlag),
('Tootsie','9576',177200000,1982,@trueFlag,@falseFlag),
('Toy Story','862',361958736,1995,@falseFlag,@trueFlag),
('Toy Story 2','863',485015179,1999,@trueFlag,@falseFlag),
('Transformers','1858',831268381,2007,@trueFlag,@falseFlag),
('Transformers: Revenge of the Fallen','8373',831268381,2009,@trueFlag,@trueFlag),
('Twister','664',494471524,1996,@trueFlag,@falseFlag),
('Up','14160',1118888979,2009,@trueFlag,@falseFlag),
('WALL-E','10681',495383652,2008,@trueFlag,@falseFlag),
('War of the Worlds','74',591739379,2005,@trueFlag,@falseFlag),
('Wedding Crashers','9522',285176741,2005,@trueFlag,@falseFlag),
('X2','740',407557613,2003,@trueFlag,@falseFlag),
('X-Men: The Last Stand','741',458751448,2000,@trueFlag,@falseFlag);

DELETE FROM [mp].[YearTopGrossingMovie];
DELETE FROM [mp].[Movie];
DELETE FROM [mp].[MovieExternalIdentifier];

INSERT INTO [mp].[MovieExternalIdentifier]
(
    [ExternalIdentifierValue]
)
SELECT DISTINCT [buffer].[TheMovieDBIdentifier]    
FROM @buffer AS [buffer];

INSERT INTO [mp].[Movie]
(
    [MovieExternalIdentifierKey],
	[Title],
	[TopGrossing],
	[WorldwideRevenue],
	[Year]
)
SELECT DISTINCT 
    [external].[MovieExternalIdentifierKey],
    [buffer].[Title],
    [buffer].[TopGrossing],
    [buffer].[WorldwideRevenue],
    [buffer].[Year]
FROM @buffer AS [buffer]
INNER JOIN [mp].[MovieExternalIdentifier] AS [external]
    ON [buffer].[TheMovieDBIdentifier] = [external].[ExternalIdentifierValue];

INSERT INTO [mp].[YearTopGrossingMovie]
(
	[MovieKey]
)
SELECT DISTINCT [m].[MovieKey] 
FROM [mp].[Movie] AS [m]
INNER JOIN @buffer AS [b]
    ON [m].[Title] = [b].[Title]
WHERE
        [b].[TopGrossOfYear] = @trueFlag;