﻿ALTER TABLE [mp].[YearCard]
    ADD CONSTRAINT [FK_YearCard_YearTopGrossingMovie] FOREIGN KEY ([MovieKey]) REFERENCES [mp].[YearTopGrossingMovie] ([MovieKey]) ON DELETE NO ACTION ON UPDATE NO ACTION;

