﻿CREATE PROCEDURE [mp].[ValidateTransactionContext]
	@transactionExpected BIT
AS
	IF 
	(
        (
            @transactionExpected = 1 
            AND 
            XACT_STATE() = 0
        )
        OR
        (
            @transactionExpected = 0
            AND
            XACT_STATE() != 0
        )
    )
    BEGIN
	    DECLARE @messageText NVARCHAR(400)
        SET @messageText = [mp].[FormatTransactionContextExceptionMessage]();
        RAISERROR(@messageText,16,1);
        RETURN;
    END
RETURN 0