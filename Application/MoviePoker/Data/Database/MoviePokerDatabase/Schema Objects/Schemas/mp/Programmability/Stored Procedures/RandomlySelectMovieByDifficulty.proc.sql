﻿CREATE PROCEDURE [mp].[RandomlySelectMovieByDifficulty]
	@difficultyQuotient INT = 1,
	@deckKey BIGINT = NULL
AS
    SET NOCOUNT ON;
    
    DECLARE
    	@movieKey BIGINT,
	    @movieYear SMALLINT;
	    
	EXECUTE [mp].[DoRandomlySelectMovieByDifficulty]
	    @difficultyQuotient = @difficultyQuotient,
	    @deckKey = @deckKey,
	    @movieKey = @movieKey OUTPUT,
	    @movieYear = @movieYear OUTPUT;
	    
	SELECT
	    [movie].[MovieKey], 
	    [movie].[MovieExternalIdentifierKey],
	    [movie].[Title],
	    [movie].[TopGrossing],
	    [movie].[WorldwideRevenue],
	    [movie].[Year]
    FROM [mp].[Movie] AS [movie]
    WHERE
            [movie].[MovieKey] = @movieKey;
	
RETURN 0;