﻿ALTER TABLE [mp].[Movie]
    ADD CONSTRAINT [PK_mp.Movie] PRIMARY KEY CLUSTERED ([MovieKey] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);

