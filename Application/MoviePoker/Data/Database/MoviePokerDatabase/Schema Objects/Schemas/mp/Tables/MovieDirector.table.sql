﻿CREATE TABLE [mp].[MovieDirector] 
(
    [MovieExternalIdentifierKey]            BIGINT          NOT NULL,
    [DirectorPersonExternalIdentifierKey]   BIGINT          NOT NULL
);

