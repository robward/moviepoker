﻿CREATE TABLE [mp].[PersonExternalIdentifier] 
(
    [PersonExternalIdentifierKey]       BIGINT          IDENTITY (0, 1) NOT NULL,
    [ExternalIdentifierValue]           VARCHAR(36)                     NOT NULL    UNIQUE NONCLUSTERED
);

