﻿CREATE PROCEDURE [mp].[DoRandomlySelectMovieByDifficulty]
	@difficultyQuotient INT = 1,
	@deckKey BIGINT = NULL,
	@movieKey BIGINT OUTPUT,
	@movieYear SMALLINT OUTPUT
AS
    SET NOCOUNT ON;
    
    DECLARE
	    @earliestYear INT,
	    @falseFlag BIT,
	    @mostRecentYear SMALLINT,
	    @startingYear SMALLINT,
	    @trueFlag BIT;
	    
	SELECT
	    @falseFlag = CONVERT(BIT,0),
	    @trueFlag = CONVERT(BIT,1);
        
    SELECT TOP 1 @earliestYear = [movie].[Year]
    FROM [mp].[Movie] AS [movie]
    WHERE
            [movie].[TopGrossing] = @trueFlag
    ORDER BY [movie].[Year] ASC;
	
    SELECT TOP 1 @mostRecentYear = [movie].[Year]
    FROM [mp].[Movie] AS [movie]
    WHERE
            [movie].[TopGrossing] = @trueFlag
    ORDER BY [movie].[Year] DESC;
	
    SELECT @startingYear =
	    @mostRecentYear -
	    (
		    (
			    @mostRecentYear - 
			    (@mostRecentYear - ((@mostRecentYear - @earliestYear) / @difficultyQuotient))
		    ) * RAND()
	    );
		
    WITH [Excluded]([MovieKey])
    AS
    (
        SELECT [movie].[MovieKey]
        FROM [mp].[Movie] AS [movie]
        INNER JOIN [mp].[MovieCard] AS [card]
            ON [movie].[MovieKey] = [card].[MovieKey]
        INNER JOIN [mp].[DeckCard] AS [deck]
            ON [card].[CardKey] = [deck].[CardKey]
        WHERE
                [deck].[DeckKey] = @deckKey
        UNION
        SELECT [movie].[MovieKey]
        FROM [mp].[Movie] AS [movie]
        INNER JOIN [mp].[YearCard] AS [card]
            ON [movie].[MovieKey] = [card].[MovieKey]
        INNER JOIN [mp].[DeckCard] AS [deck]
            ON [card].[CardKey] = [deck].[CardKey]
        WHERE
                [deck].[DeckKey] = @deckKey
        
    ),
    [KeyOffset] ([MovieKey],[Offset])
    AS
    (
	    SELECT
		    [movie].[MovieKey],
		    ([movie].[Year] - @startingYear)
	    FROM [mp].[Movie] AS [movie]
	    LEFT OUTER JOIN [Excluded] AS [excluded]
	        ON [movie].[MovieKey] = [excluded].[MovieKey]
	    WHERE
			    [movie].[TopGrossing] = @trueFlag
		    AND	[movie].[Year] >= @startingYear
		    AND [excluded].[MovieKey] IS NULL
    ),
    [SmallestOffset] ([Offset])
    AS
    (
	    SELECT MIN([o].[Offset])
	    FROM [KeyOffset] AS [o]
    ),
    [Candidate] ([MovieKey],[Index])
    AS
    (
	    SELECT
		    [offset].[MovieKey],
		    ROW_NUMBER() OVER(ORDER BY [offset].[MovieKey])
	    FROM [KeyOffset] AS [offset]
	    INNER JOIN [SmallestOffset] AS [smallest]
		    ON [offset].[Offset] = [smallest].[Offset]
    ),
    [TopIndex]([Index])
    AS
    (
	    SELECT MAX([candidate].[Index])
	    FROM [Candidate] AS [candidate]
    ),
    [SelectedIndex]([Index])
    AS
    (
	    SELECT CONVERT(INT,(([top].[Index] + 1) - 1) * RAND() + 1)
	    FROM [TopIndex] AS [top]
    )
    SELECT
	    @movieKey = [movie].[MovieKey], 
	    @movieYear = [movie].[Year]
    FROM [mp].[Movie] AS [movie]
    INNER JOIN [Candidate] AS [candidate]
	    ON [movie].[MovieKey] = [candidate].[MovieKey]
    INNER JOIN [SelectedIndex] AS [index]
	    ON [candidate].[Index] = [index].[Index];
	
RETURN 0;