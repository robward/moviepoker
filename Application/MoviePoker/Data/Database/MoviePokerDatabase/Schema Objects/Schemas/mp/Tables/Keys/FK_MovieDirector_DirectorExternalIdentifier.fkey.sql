﻿ALTER TABLE [mp].[MovieDirector]
    ADD CONSTRAINT [FK_MovieDirector_DirectorExternalIdentifier] FOREIGN KEY ([DirectorPersonExternalIdentifierKey]) REFERENCES [mp].[PersonExternalIdentifier] ([PersonExternalIdentifierKey]) ON DELETE NO ACTION ON UPDATE NO ACTION;

