﻿CREATE TABLE [mp].[Movie] 
(
    [MovieKey]                      BIGINT          IDENTITY (0, 1) NOT NULL,
    [MovieExternalIdentifierKey]    BIGINT                          NOT NULL,
    [Title]                         NVARCHAR(400)                   NOT NULL,
    [TopGrossing]                   BIT                             NOT NULL    DEFAULT(0),
    [Year]                          SMALLINT                        NOT NULL,
    [WorldwideRevenue]              BIGINT                          NULL
);

