﻿CREATE TABLE [mp].[MovieCard] 
(
    [CardKey]   BIGINT NOT NULL,
    [MovieKey]  BIGINT NOT NULL UNIQUE NONCLUSTERED              
);

