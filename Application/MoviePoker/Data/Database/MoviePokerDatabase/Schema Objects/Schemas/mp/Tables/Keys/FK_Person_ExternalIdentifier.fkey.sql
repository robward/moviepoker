﻿ALTER TABLE [mp].[Person]
    ADD CONSTRAINT [FK_Person_ExternalIdentifier] FOREIGN KEY ([PersonExternalIdentifierKey]) REFERENCES [mp].[PersonExternalIdentifier] ([PersonExternalIdentifierKey]) ON DELETE NO ACTION ON UPDATE NO ACTION;

