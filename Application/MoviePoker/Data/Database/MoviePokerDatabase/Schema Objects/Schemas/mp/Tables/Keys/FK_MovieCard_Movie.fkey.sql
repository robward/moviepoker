﻿ALTER TABLE [mp].[MovieCard]
    ADD CONSTRAINT [FK_MovieCard_Movie] FOREIGN KEY ([MovieKey]) REFERENCES [mp].[Movie] ([MovieKey]) ON DELETE NO ACTION ON UPDATE NO ACTION;

