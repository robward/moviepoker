﻿ALTER TABLE [mp].[MovieActor]
    ADD CONSTRAINT [FK_MovieActor_MovieExternalIdentifier] FOREIGN KEY ([MovieExternalIdentifierKey]) REFERENCES [mp].[MovieExternalIdentifier] ([MovieExternalIdentifierKey]) ON DELETE NO ACTION ON UPDATE NO ACTION;

