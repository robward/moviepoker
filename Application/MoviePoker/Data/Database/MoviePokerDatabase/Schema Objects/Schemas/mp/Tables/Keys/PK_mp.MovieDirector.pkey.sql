﻿ALTER TABLE [mp].[MovieDirector]
    ADD CONSTRAINT [PK_mp.MovieDirector] PRIMARY KEY CLUSTERED ([MovieExternalIdentifierKey] ASC, [DirectorPersonExternalIdentifierKey] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);

