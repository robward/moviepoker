﻿CREATE TABLE [mp].[Person] 
(
    [PersonKey]                     BIGINT          IDENTITY (0, 1) NOT NULL,
    [PersonExternalIdentifierKey]   BIGINT                          NOT NULL,
    [Name]                          NVARCHAR(400)                   NOT NULL
);

