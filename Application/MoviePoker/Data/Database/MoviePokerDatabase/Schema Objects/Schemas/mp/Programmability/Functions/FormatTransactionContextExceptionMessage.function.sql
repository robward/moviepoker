﻿CREATE FUNCTION [mp].[FormatTransactionContextExceptionMessage]()
RETURNS NVARCHAR(400)
AS
BEGIN
	RETURN N'Invalid transaction context.';
END