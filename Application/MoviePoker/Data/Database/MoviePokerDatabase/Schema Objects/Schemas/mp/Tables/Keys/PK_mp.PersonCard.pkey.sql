﻿ALTER TABLE [mp].[PersonCard]
    ADD CONSTRAINT [PK_mp.PersonCard] PRIMARY KEY CLUSTERED ([CardKey] ASC, [PersonKey] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);

