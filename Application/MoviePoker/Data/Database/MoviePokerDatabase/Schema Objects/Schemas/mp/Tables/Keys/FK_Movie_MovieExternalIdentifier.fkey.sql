﻿ALTER TABLE [mp].[Movie]
    ADD CONSTRAINT [FK_Movie_ExternalIdentifier] FOREIGN KEY ([MovieExternalIdentifierKey]) REFERENCES [mp].[MovieExternalIdentifier] ([MovieExternalIdentifierKey]) ON DELETE NO ACTION ON UPDATE NO ACTION;

