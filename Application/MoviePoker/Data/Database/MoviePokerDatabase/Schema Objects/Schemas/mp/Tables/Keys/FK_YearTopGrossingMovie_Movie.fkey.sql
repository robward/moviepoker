﻿ALTER TABLE [mp].[YearTopGrossingMovie]
    ADD CONSTRAINT [FK_YearTopGrossingMovie_Movie] FOREIGN KEY ([MovieKey]) REFERENCES [mp].[Movie] ([MovieKey]) ON DELETE NO ACTION ON UPDATE NO ACTION;

