﻿ALTER TABLE [mp].[DeckCard]
	ADD CONSTRAINT [DeckIndex] 
	UNIQUE NONCLUSTERED
	(
	    [DeckKey],
	    [Index]
	)
