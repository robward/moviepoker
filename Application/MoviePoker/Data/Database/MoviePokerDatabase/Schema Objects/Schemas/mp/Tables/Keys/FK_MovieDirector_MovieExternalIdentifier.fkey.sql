﻿ALTER TABLE [mp].[MovieDirector]
    ADD CONSTRAINT [FK_MovieDirector_MovieExternalIdentifier] FOREIGN KEY ([MovieExternalIdentifierKey]) REFERENCES [mp].[MovieExternalIdentifier] ([MovieExternalIdentifierKey]) ON DELETE NO ACTION ON UPDATE NO ACTION;

