﻿ALTER TABLE [mp].[MovieActor]
    ADD CONSTRAINT [FK_MovieActor_ActorExternalIdentifier] FOREIGN KEY ([ActorPersonExternalIdentifierKey]) REFERENCES [mp].[PersonExternalIdentifier] ([PersonExternalIdentifierKey]) ON DELETE NO ACTION ON UPDATE NO ACTION;

