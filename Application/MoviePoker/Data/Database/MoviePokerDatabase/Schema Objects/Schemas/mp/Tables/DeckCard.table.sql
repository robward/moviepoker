﻿CREATE TABLE [mp].[DeckCard] 
(
    [DeckKey]   BIGINT NOT NULL,
    [CardKey]   BIGINT NOT NULL,
    [Index]     TINYINT NOT NULL CHECK([Index] >= 0 AND [Index] <= 52)
);

