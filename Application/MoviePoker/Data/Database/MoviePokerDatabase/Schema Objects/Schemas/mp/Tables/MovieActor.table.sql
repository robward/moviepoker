﻿CREATE TABLE [mp].[MovieActor] 
(
    [MovieExternalIdentifierKey]        BIGINT          NOT NULL,
    [ActorPersonExternalIdentifierKey]  BIGINT          NOT NULL
);

