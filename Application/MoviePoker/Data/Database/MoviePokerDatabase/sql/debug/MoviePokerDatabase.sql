﻿/*
Deployment script for MoviePoker
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "MoviePoker"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL10.MSSQLSERVER\MSSQL\DATA\"

GO
USE [master]

GO
:on error exit
GO
IF (DB_ID(N'$(DatabaseName)') IS NOT NULL
    AND DATABASEPROPERTYEX(N'$(DatabaseName)','Status') <> N'ONLINE')
BEGIN
    RAISERROR(N'The state of the target database, %s, is not set to ONLINE. To deploy to this database, its state must be set to ONLINE.', 16, 127,N'$(DatabaseName)') WITH NOWAIT
    RETURN
END

GO
IF (DB_ID(N'$(DatabaseName)') IS NOT NULL) 
BEGIN
    ALTER DATABASE [$(DatabaseName)]
    SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
    DROP DATABASE [$(DatabaseName)];
END

GO
PRINT N'Creating $(DatabaseName)...'
GO
CREATE DATABASE [$(DatabaseName)]
    ON 
    PRIMARY(NAME = [MoviePoker], FILENAME = '$(DefaultDataPath)$(DatabaseName).mdf', MAXSIZE = UNLIMITED, FILEGROWTH = 1024 KB)
    LOG ON (NAME = [MoviePoker_log], FILENAME = '$(DefaultDataPath)$(DatabaseName)_log.ldf', MAXSIZE = 2097152 MB, FILEGROWTH = 10 %) COLLATE SQL_Latin1_General_CP1_CI_AS
GO
EXECUTE sp_dbcmptlevel [$(DatabaseName)], 100;


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET ANSI_NULLS ON,
                ANSI_PADDING ON,
                ANSI_WARNINGS ON,
                ARITHABORT ON,
                CONCAT_NULL_YIELDS_NULL ON,
                NUMERIC_ROUNDABORT OFF,
                QUOTED_IDENTIFIER ON,
                ANSI_NULL_DEFAULT ON,
                CURSOR_DEFAULT LOCAL,
                RECOVERY FULL,
                CURSOR_CLOSE_ON_COMMIT OFF,
                AUTO_CREATE_STATISTICS ON,
                AUTO_SHRINK OFF,
                AUTO_UPDATE_STATISTICS ON,
                RECURSIVE_TRIGGERS OFF 
            WITH ROLLBACK IMMEDIATE;
        ALTER DATABASE [$(DatabaseName)]
            SET AUTO_CLOSE OFF 
            WITH ROLLBACK IMMEDIATE;
    END


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET ALLOW_SNAPSHOT_ISOLATION ON;
    END


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET READ_COMMITTED_SNAPSHOT OFF;
    END


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET AUTO_UPDATE_STATISTICS_ASYNC OFF,
                PAGE_VERIFY NONE,
                DATE_CORRELATION_OPTIMIZATION OFF,
                DISABLE_BROKER,
                PARAMETERIZATION SIMPLE 
            WITH ROLLBACK IMMEDIATE;
    END


GO
IF IS_SRVROLEMEMBER(N'sysadmin') = 1
    BEGIN
        IF EXISTS (SELECT 1
                   FROM   [master].[dbo].[sysdatabases]
                   WHERE  [name] = N'$(DatabaseName)')
            BEGIN
                EXECUTE sp_executesql N'ALTER DATABASE [$(DatabaseName)]
    SET TRUSTWORTHY OFF,
        DB_CHAINING OFF 
    WITH ROLLBACK IMMEDIATE';
            END
    END
ELSE
    BEGIN
        PRINT N'The database settings for DB_CHAINING or TRUSTWORTHY cannot be modified. You must be a SysAdmin to apply these settings.';
    END


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET HONOR_BROKER_PRIORITY OFF 
            WITH ROLLBACK IMMEDIATE;
    END


GO
USE [$(DatabaseName)]

GO
IF fulltextserviceproperty(N'IsFulltextInstalled') = 1
    EXECUTE sp_fulltext_database 'enable';


GO

GO
/*
 Pre-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be executed before the build script.	
 Use SQLCMD syntax to include a file in the pre-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the pre-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

GO

GO
PRINT N'Creating mp...';


GO
CREATE SCHEMA [mp]
    AUTHORIZATION [dbo];


GO
PRINT N'Creating mp.Card...';


GO
CREATE TABLE [mp].[Card] (
    [CardKey] BIGINT IDENTITY (0, 1) NOT NULL
);


GO
PRINT N'Creating mp.Deck...';


GO
CREATE TABLE [mp].[Deck] (
    [DeckKey] BIGINT IDENTITY (0, 1) NOT NULL
);


GO
PRINT N'Creating mp.DeckCard...';


GO
CREATE TABLE [mp].[DeckCard] (
    [DeckKey] BIGINT  NOT NULL,
    [CardKey] BIGINT  NOT NULL,
    [Index]   TINYINT NOT NULL
);


GO
PRINT N'Creating mp.Movie...';


GO
CREATE TABLE [mp].[Movie] (
    [MovieKey]                   BIGINT         IDENTITY (0, 1) NOT NULL,
    [MovieExternalIdentifierKey] BIGINT         NOT NULL,
    [Title]                      NVARCHAR (400) NOT NULL,
    [TopGrossing]                BIT            NOT NULL,
    [Year]                       SMALLINT       NOT NULL,
    [WorldwideRevenue]           BIGINT         NULL
);


GO
PRINT N'Creating mp.MovieActor...';


GO
CREATE TABLE [mp].[MovieActor] (
    [MovieExternalIdentifierKey]       BIGINT NOT NULL,
    [ActorPersonExternalIdentifierKey] BIGINT NOT NULL
);


GO
PRINT N'Creating mp.MovieCard...';


GO
CREATE TABLE [mp].[MovieCard] (
    [CardKey]  BIGINT NOT NULL,
    [MovieKey] BIGINT NOT NULL,
    UNIQUE NONCLUSTERED ([MovieKey] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
);


GO
PRINT N'Creating mp.MovieDirector...';


GO
CREATE TABLE [mp].[MovieDirector] (
    [MovieExternalIdentifierKey]          BIGINT NOT NULL,
    [DirectorPersonExternalIdentifierKey] BIGINT NOT NULL
);


GO
PRINT N'Creating mp.MovieExternalIdentifier...';


GO
CREATE TABLE [mp].[MovieExternalIdentifier] (
    [MovieExternalIdentifierKey] BIGINT       IDENTITY (0, 1) NOT NULL,
    [ExternalIdentifierValue]    VARCHAR (36) NOT NULL,
    UNIQUE NONCLUSTERED ([ExternalIdentifierValue] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
);


GO
PRINT N'Creating mp.Person...';


GO
CREATE TABLE [mp].[Person] (
    [PersonKey]                   BIGINT         IDENTITY (0, 1) NOT NULL,
    [PersonExternalIdentifierKey] BIGINT         NOT NULL,
    [Name]                        NVARCHAR (400) NOT NULL
);


GO
PRINT N'Creating mp.PersonCard...';


GO
CREATE TABLE [mp].[PersonCard] (
    [CardKey]   BIGINT NOT NULL,
    [PersonKey] BIGINT NOT NULL,
    UNIQUE NONCLUSTERED ([PersonKey] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
);


GO
PRINT N'Creating mp.PersonExternalIdentifier...';


GO
CREATE TABLE [mp].[PersonExternalIdentifier] (
    [PersonExternalIdentifierKey] BIGINT       IDENTITY (0, 1) NOT NULL,
    [ExternalIdentifierValue]     VARCHAR (36) NOT NULL,
    UNIQUE NONCLUSTERED ([ExternalIdentifierValue] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
);


GO
PRINT N'Creating mp.YearCard...';


GO
CREATE TABLE [mp].[YearCard] (
    [CardKey]  BIGINT NOT NULL,
    [MovieKey] BIGINT NOT NULL,
    UNIQUE NONCLUSTERED ([MovieKey] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
);


GO
PRINT N'Creating mp.YearTopGrossingMovie...';


GO
CREATE TABLE [mp].[YearTopGrossingMovie] (
    [MovieKey] BIGINT NOT NULL
);


GO
PRINT N'Creating mp.Movie.MovieByYearTopGrossing...';


GO
CREATE NONCLUSTERED INDEX [MovieByYearTopGrossing]
    ON [mp].[Movie]([Year] ASC, [TopGrossing] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0);


GO
PRINT N'Creating On column: TopGrossing...';


GO
ALTER TABLE [mp].[Movie]
    ADD DEFAULT (0) FOR [TopGrossing];


GO
PRINT N'Creating mp.DeckIndex...';


GO
ALTER TABLE [mp].[DeckCard]
    ADD CONSTRAINT [DeckIndex] UNIQUE NONCLUSTERED ([DeckKey] ASC, [Index] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating mp.PK_mp.Card...';


GO
ALTER TABLE [mp].[Card]
    ADD CONSTRAINT [PK_mp.Card] PRIMARY KEY CLUSTERED ([CardKey] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating mp.PK_mp.Deck...';


GO
ALTER TABLE [mp].[Deck]
    ADD CONSTRAINT [PK_mp.Deck] PRIMARY KEY CLUSTERED ([DeckKey] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating mp.PK_mp.DeckCard...';


GO
ALTER TABLE [mp].[DeckCard]
    ADD CONSTRAINT [PK_mp.DeckCard] PRIMARY KEY CLUSTERED ([DeckKey] ASC, [CardKey] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating mp.PK_mp.Movie...';


GO
ALTER TABLE [mp].[Movie]
    ADD CONSTRAINT [PK_mp.Movie] PRIMARY KEY CLUSTERED ([MovieKey] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating mp.PK_mp.MovieActor...';


GO
ALTER TABLE [mp].[MovieActor]
    ADD CONSTRAINT [PK_mp.MovieActor] PRIMARY KEY CLUSTERED ([MovieExternalIdentifierKey] ASC, [ActorPersonExternalIdentifierKey] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating mp.PK_mp.MovieCard...';


GO
ALTER TABLE [mp].[MovieCard]
    ADD CONSTRAINT [PK_mp.MovieCard] PRIMARY KEY CLUSTERED ([CardKey] ASC, [MovieKey] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating mp.PK_mp.MovieDirector...';


GO
ALTER TABLE [mp].[MovieDirector]
    ADD CONSTRAINT [PK_mp.MovieDirector] PRIMARY KEY CLUSTERED ([MovieExternalIdentifierKey] ASC, [DirectorPersonExternalIdentifierKey] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating mp.PK_mp.MovieExternalIdentifier...';


GO
ALTER TABLE [mp].[MovieExternalIdentifier]
    ADD CONSTRAINT [PK_mp.MovieExternalIdentifier] PRIMARY KEY CLUSTERED ([MovieExternalIdentifierKey] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating mp.PK_mp.Person...';


GO
ALTER TABLE [mp].[Person]
    ADD CONSTRAINT [PK_mp.Person] PRIMARY KEY CLUSTERED ([PersonKey] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating mp.PK_mp.PersonCard...';


GO
ALTER TABLE [mp].[PersonCard]
    ADD CONSTRAINT [PK_mp.PersonCard] PRIMARY KEY CLUSTERED ([CardKey] ASC, [PersonKey] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating mp.PK_mp.PersonExternalIdentifier...';


GO
ALTER TABLE [mp].[PersonExternalIdentifier]
    ADD CONSTRAINT [PK_mp.PersonExternalIdentifier] PRIMARY KEY CLUSTERED ([PersonExternalIdentifierKey] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating mp.PK_mp.YearCard...';


GO
ALTER TABLE [mp].[YearCard]
    ADD CONSTRAINT [PK_mp.YearCard] PRIMARY KEY CLUSTERED ([CardKey] ASC, [MovieKey] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating mp.PK_mp.YearTopGrossingMovie...';


GO
ALTER TABLE [mp].[YearTopGrossingMovie]
    ADD CONSTRAINT [PK_mp.YearTopGrossingMovie] PRIMARY KEY CLUSTERED ([MovieKey] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating mp.FK_DeckCard_Card...';


GO
ALTER TABLE [mp].[DeckCard]
    ADD CONSTRAINT [FK_DeckCard_Card] FOREIGN KEY ([CardKey]) REFERENCES [mp].[Card] ([CardKey]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating mp.FK_DeckCard_Deck...';


GO
ALTER TABLE [mp].[DeckCard]
    ADD CONSTRAINT [FK_DeckCard_Deck] FOREIGN KEY ([DeckKey]) REFERENCES [mp].[Deck] ([DeckKey]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating mp.FK_Movie_ExternalIdentifier...';


GO
ALTER TABLE [mp].[Movie]
    ADD CONSTRAINT [FK_Movie_ExternalIdentifier] FOREIGN KEY ([MovieExternalIdentifierKey]) REFERENCES [mp].[MovieExternalIdentifier] ([MovieExternalIdentifierKey]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating mp.FK_MovieActor_ActorExternalIdentifier...';


GO
ALTER TABLE [mp].[MovieActor]
    ADD CONSTRAINT [FK_MovieActor_ActorExternalIdentifier] FOREIGN KEY ([ActorPersonExternalIdentifierKey]) REFERENCES [mp].[PersonExternalIdentifier] ([PersonExternalIdentifierKey]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating mp.FK_MovieActor_MovieExternalIdentifier...';


GO
ALTER TABLE [mp].[MovieActor]
    ADD CONSTRAINT [FK_MovieActor_MovieExternalIdentifier] FOREIGN KEY ([MovieExternalIdentifierKey]) REFERENCES [mp].[MovieExternalIdentifier] ([MovieExternalIdentifierKey]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating mp.FK_MovieCard_Card...';


GO
ALTER TABLE [mp].[MovieCard]
    ADD CONSTRAINT [FK_MovieCard_Card] FOREIGN KEY ([CardKey]) REFERENCES [mp].[Card] ([CardKey]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating mp.FK_MovieCard_Movie...';


GO
ALTER TABLE [mp].[MovieCard]
    ADD CONSTRAINT [FK_MovieCard_Movie] FOREIGN KEY ([MovieKey]) REFERENCES [mp].[Movie] ([MovieKey]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating mp.FK_MovieDirector_DirectorExternalIdentifier...';


GO
ALTER TABLE [mp].[MovieDirector]
    ADD CONSTRAINT [FK_MovieDirector_DirectorExternalIdentifier] FOREIGN KEY ([DirectorPersonExternalIdentifierKey]) REFERENCES [mp].[PersonExternalIdentifier] ([PersonExternalIdentifierKey]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating mp.FK_MovieDirector_MovieExternalIdentifier...';


GO
ALTER TABLE [mp].[MovieDirector]
    ADD CONSTRAINT [FK_MovieDirector_MovieExternalIdentifier] FOREIGN KEY ([MovieExternalIdentifierKey]) REFERENCES [mp].[MovieExternalIdentifier] ([MovieExternalIdentifierKey]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating mp.FK_Person_ExternalIdentifier...';


GO
ALTER TABLE [mp].[Person]
    ADD CONSTRAINT [FK_Person_ExternalIdentifier] FOREIGN KEY ([PersonExternalIdentifierKey]) REFERENCES [mp].[PersonExternalIdentifier] ([PersonExternalIdentifierKey]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating mp.FK_PersonCard_Card...';


GO
ALTER TABLE [mp].[PersonCard]
    ADD CONSTRAINT [FK_PersonCard_Card] FOREIGN KEY ([CardKey]) REFERENCES [mp].[Card] ([CardKey]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating mp.FK_PersonCard_Person...';


GO
ALTER TABLE [mp].[PersonCard]
    ADD CONSTRAINT [FK_PersonCard_Person] FOREIGN KEY ([PersonKey]) REFERENCES [mp].[Person] ([PersonKey]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating mp.FK_YearCard_Card...';


GO
ALTER TABLE [mp].[YearCard]
    ADD CONSTRAINT [FK_YearCard_Card] FOREIGN KEY ([CardKey]) REFERENCES [mp].[Card] ([CardKey]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating mp.FK_YearCard_YearTopGrossingMovie...';


GO
ALTER TABLE [mp].[YearCard]
    ADD CONSTRAINT [FK_YearCard_YearTopGrossingMovie] FOREIGN KEY ([MovieKey]) REFERENCES [mp].[YearTopGrossingMovie] ([MovieKey]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating mp.FK_YearTopGrossingMovie_Movie...';


GO
ALTER TABLE [mp].[YearTopGrossingMovie]
    ADD CONSTRAINT [FK_YearTopGrossingMovie_Movie] FOREIGN KEY ([MovieKey]) REFERENCES [mp].[Movie] ([MovieKey]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating On column: Index...';


GO
ALTER TABLE [mp].[DeckCard]
    ADD CHECK ([Index] >= 0 AND [Index] <= 52);


GO
PRINT N'Creating mp.DoRandomlySelectMovieByDifficulty...';


GO
CREATE PROCEDURE [mp].[DoRandomlySelectMovieByDifficulty]
@difficultyQuotient INT=1, @deckKey BIGINT=NULL, @movieKey BIGINT OUTPUT, @movieYear SMALLINT OUTPUT
AS
SET NOCOUNT ON;
    
    DECLARE
	    @earliestYear INT,
	    @falseFlag BIT,
	    @mostRecentYear SMALLINT,
	    @startingYear SMALLINT,
	    @trueFlag BIT;
	    
	SELECT
	    @falseFlag = CONVERT(BIT,0),
	    @trueFlag = CONVERT(BIT,1);
        
    SELECT TOP 1 @earliestYear = [movie].[Year]
    FROM [mp].[Movie] AS [movie]
    WHERE
            [movie].[TopGrossing] = @trueFlag
    ORDER BY [movie].[Year] ASC;
	
    SELECT TOP 1 @mostRecentYear = [movie].[Year]
    FROM [mp].[Movie] AS [movie]
    WHERE
            [movie].[TopGrossing] = @trueFlag
    ORDER BY [movie].[Year] DESC;
	
    SELECT @startingYear =
	    @mostRecentYear -
	    (
		    (
			    @mostRecentYear - 
			    (@mostRecentYear - ((@mostRecentYear - @earliestYear) / @difficultyQuotient))
		    ) * RAND()
	    );
		
    WITH [Excluded]([MovieKey])
    AS
    (
        SELECT [movie].[MovieKey]
        FROM [mp].[Movie] AS [movie]
        INNER JOIN [mp].[MovieCard] AS [card]
            ON [movie].[MovieKey] = [card].[MovieKey]
        INNER JOIN [mp].[DeckCard] AS [deck]
            ON [card].[CardKey] = [deck].[CardKey]
        WHERE
                [deck].[DeckKey] = @deckKey
        UNION
        SELECT [movie].[MovieKey]
        FROM [mp].[Movie] AS [movie]
        INNER JOIN [mp].[YearCard] AS [card]
            ON [movie].[MovieKey] = [card].[MovieKey]
        INNER JOIN [mp].[DeckCard] AS [deck]
            ON [card].[CardKey] = [deck].[CardKey]
        WHERE
                [deck].[DeckKey] = @deckKey
        
    ),
    [KeyOffset] ([MovieKey],[Offset])
    AS
    (
	    SELECT
		    [movie].[MovieKey],
		    ([movie].[Year] - @startingYear)
	    FROM [mp].[Movie] AS [movie]
	    LEFT OUTER JOIN [Excluded] AS [excluded]
	        ON [movie].[MovieKey] = [excluded].[MovieKey]
	    WHERE
			    [movie].[TopGrossing] = @trueFlag
		    AND	[movie].[Year] >= @startingYear
		    AND [excluded].[MovieKey] IS NULL
    ),
    [SmallestOffset] ([Offset])
    AS
    (
	    SELECT MIN([o].[Offset])
	    FROM [KeyOffset] AS [o]
    ),
    [Candidate] ([MovieKey],[Index])
    AS
    (
	    SELECT
		    [offset].[MovieKey],
		    ROW_NUMBER() OVER(ORDER BY [offset].[MovieKey])
	    FROM [KeyOffset] AS [offset]
	    INNER JOIN [SmallestOffset] AS [smallest]
		    ON [offset].[Offset] = [smallest].[Offset]
    ),
    [TopIndex]([Index])
    AS
    (
	    SELECT MAX([candidate].[Index])
	    FROM [Candidate] AS [candidate]
    ),
    [SelectedIndex]([Index])
    AS
    (
	    SELECT CONVERT(INT,(([top].[Index] + 1) - 1) * RAND() + 1)
	    FROM [TopIndex] AS [top]
    )
    SELECT
	    @movieKey = [movie].[MovieKey], 
	    @movieYear = [movie].[Year]
    FROM [mp].[Movie] AS [movie]
    INNER JOIN [Candidate] AS [candidate]
	    ON [movie].[MovieKey] = [candidate].[MovieKey]
    INNER JOIN [SelectedIndex] AS [index]
	    ON [candidate].[Index] = [index].[Index];
	
RETURN 0;


GO
PRINT N'Creating mp.RandomlySelectMovieByDifficulty...';


GO
CREATE PROCEDURE [mp].[RandomlySelectMovieByDifficulty]
@difficultyQuotient INT=1, @deckKey BIGINT=NULL
AS
SET NOCOUNT ON;
    
    DECLARE
    	@movieKey BIGINT,
	    @movieYear SMALLINT;
	    
	EXECUTE [mp].[DoRandomlySelectMovieByDifficulty]
	    @difficultyQuotient = @difficultyQuotient,
	    @deckKey = @deckKey,
	    @movieKey = @movieKey OUTPUT,
	    @movieYear = @movieYear OUTPUT;
	    
	SELECT
	    [movie].[MovieKey], 
	    [movie].[MovieExternalIdentifierKey],
	    [movie].[Title],
	    [movie].[TopGrossing],
	    [movie].[WorldwideRevenue],
	    [movie].[Year]
    FROM [mp].[Movie] AS [movie]
    WHERE
            [movie].[MovieKey] = @movieKey;
	
RETURN 0;


GO
PRINT N'Creating mp.FormatTransactionContextExceptionMessage...';


GO
CREATE FUNCTION [mp].[FormatTransactionContextExceptionMessage]
( )
RETURNS NVARCHAR (400)
AS
BEGIN
	RETURN N'Invalid transaction context.';
END


GO
PRINT N'Creating mp.ValidateTransactionContext...';


GO
CREATE PROCEDURE [mp].[ValidateTransactionContext]
@transactionExpected BIT
AS
IF 
	(
        (
            @transactionExpected = 1 
            AND 
            XACT_STATE() = 0
        )
        OR
        (
            @transactionExpected = 0
            AND
            XACT_STATE() != 0
        )
    )
    BEGIN
	    DECLARE @messageText NVARCHAR(400)
        SET @messageText = [mp].[FormatTransactionContextExceptionMessage]();
        RAISERROR(@messageText,16,1);
        RETURN;
    END
RETURN 0


GO

GO
/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
DECLARE 
    @trueFlag BIT,
    @falseFlag BIT;
    
SELECT 
    @trueFlag = CONVERT(BIT,1),
    @falseFlag = CONVERT(BIT,0);


DECLARE @buffer TABLE
(
	[Title]	NVARCHAR(400),
	[TheMovieDBIdentifier] VARCHAR(16),
	[WorldwideRevenue] BIGINT,
	[Year] SMALLINT,
	[TopGrossing] BIT,
	[TopGrossOfYear] BIT
);

INSERT into @buffer
(
	[Title],
	[TheMovieDBIdentifier],
	[WorldwideRevenue],
	[Year],
	[TopGrossing],
	[TopGrossOfYear]
)
VALUES
('101 Dalmations','12230',38562000,1961,@falseFlag,@trueFlag),
('300','1271',422610419,2007,@trueFlag,@falseFlag),
('Aladdin','812',504050219,1992,@trueFlag,@trueFlag),
('Alvin and the Chipmunks','6477',353435370,2007,@trueFlag,@falseFlag),
('Armageddon','95',553709783,1998,@trueFlag,@falseFlag),
('Austin Powers in Goldmember','818',296633907,2002,@trueFlag,@falseFlag),
('Austin Powers: The Spy Who Shagged Me','817',310940086,1999,@trueFlag,@falseFlag),
('Back to the Future','105',381109762,1985,@trueFlag,@trueFlag),
('Bambi','3170',103037700,1942,@falseFlag,@trueFlag),
('Batman','268',413200000,1989,@trueFlag,@trueFlag),
('Batman Begins','272',371853783,2005,@trueFlag,@falseFlag),
('Ben-Hur','665',36992088,1959,@falseFlag,@trueFlag),
('Beverly Hills Cop','90',316360478,1984,@trueFlag,@falseFlag),
('Bridge on the River Kwai','1026',17195000,1957,@falseFlag,@trueFlag),
('Bruce Almighty','310',484572835,2003,@trueFlag,@falseFlag),
('Butch Cassidy and the Sundance Kid','642',102308889,1969,@trueFlag,@trueFlag),
('Cars','920',461979982,2006,@trueFlag,@falseFlag),
('Cast Away','8358',429632142,2000,@trueFlag,@falseFlag),
('Charlie and the Chocolate Factory','118',473000000,2005,@trueFlag,@falseFlag),
('Cinderella','11224',89569660,1950,@trueFlag,@trueFlag),
('Close Encounters of the Third Kind','840',303788635,1977,@trueFlag,@falseFlag),
('Doctor Zhivago','907',111721910,1965,@trueFlag,@trueFlag),
('E.T. the Extra-Terrestrial','601',792910554,1982,@trueFlag,@trueFlag),
('Fantasia','756',76000000,1940,@trueFlag,@trueFlag),
('Fiddler on the Roof','14811',38251196,1971,@falseFlag,@trueFlag),
('Finding Nemo','12',864625978,2003,@trueFlag,@falseFlag),
('Forrest Gump','13',679400000,1994,@trueFlag,@trueFlag),
('Ghost','251',505000000,1990,@trueFlag,@falseFlag),
('Ghostbusters','620',291632124,1984,@trueFlag,@trueFlag),
('Goldfinger','658',124900000,1964,@trueFlag,@trueFlag),
('Gone with the Wind','770',400176459,1939,@trueFlag,@trueFlag),
('The Graduate','730',44090729,1967,@falseFlag,@trueFlag),
('Grease','621',96300000,1978,@falseFlag,@trueFlag),
('Hancock','8960',624029371,2008,@trueFlag,@falseFlag),
('Happy Feet','9836',384334303,2006,@trueFlag,@falseFlag),
('Harry Potter and the Chamber of Secrets','672',876688482,2002,@trueFlag,@falseFlag),
('Harry Potter and the Goblet of Fire','674',892213036,2005,@trueFlag,@falseFlag),
('Harry Potter and the Prisoner of Azkaban','673',789804554,2004,@trueFlag,@falseFlag),
('Harry Potter and the Sorcerer''s Stone','671',976475550,2001,@trueFlag,@trueFlag),
('Hawaii','14689',15553018,1966,@falseFlag,@trueFlag),
('Home Alone','771',477561243,1990,@trueFlag,@trueFlag),
('How the Grinch Stole Christmas','8871',345141403,2000,@falseFlag,@trueFlag),
('How the West was Won','11897',20932883,1962,@falseFlag,@trueFlag),
('I Am Legend','6479',583184161,2007,@trueFlag,@falseFlag),
('Ice Age: The Meltdown','950',647260749,2002,@trueFlag,@falseFlag),
('Independence Day','602',816969268,1996,@trueFlag,@trueFlag),
('Indiana Jones and the Kingdom of the Crystal Skull','217',520141229,2008,@trueFlag,@falseFlag),
('Indiana Jones and the Last Crusade','89',474000000,1989,@trueFlag,@falseFlag),
('Indiana Jones and the Temple of Doom','87',333000000,1984,@trueFlag,@falseFlag),
('Iron Man','1726',460028823,2008,@trueFlag,@falseFlag),
('Jaws','578',470653000,1975,@trueFlag,@trueFlag),
('Jurassic Park','329',920100000,1993,@trueFlag,@trueFlag),
('King Kong','254',550000000,2005,@trueFlag,@trueFlag),
('Kramer vs. Kramer','12102',59986335,1979,@falseFlag,@trueFlag),
('Kung Fu Panda','9502',631619244,2008,@trueFlag,@falseFlag),
('Lady and the Tramp','10340',40249000,1955,@falseFlag,@trueFlag),
('Lawrence of Arabia','947',44824144,1962,@trueFlag,@trueFlag),
('Love Story','20493',5000000,1970,@trueFlag,@trueFlag),
('Mary Poppins','433',102272727,1964,@trueFlag,@falseFlag),
('Meet the Fockers','693',516533043,2004,@trueFlag,@falseFlag),
('Men in Black','607',589390539,1997,@trueFlag,@falseFlag),
('Mission: Impossible II','955',285176741,2000,@trueFlag,@falseFlag),
('Monsters, Inc.','585',524000000,2001,@trueFlag,@falseFlag),
('Mrs. Doubtfire','788',441195243,1993,@trueFlag,@falseFlag),
('My Big Fat Greek Wedding','8346',368744044,2002,@trueFlag,@falseFlag),
('National Lampoon''s Animal House','8469',141000000,1978,@trueFlag,@trueFlag),
('National Treasure: Book of Secrets','6637',457363168,2007,@trueFlag,@falseFlag),
('Night at the Museum','1593',549736156,2006,@trueFlag,@falseFlag),
('One Flew Over the Cuckoo''s Nest','510',108981275,1975,@trueFlag,@falseFlag),
('Pearl Harbor','676',449220945,2001,@trueFlag,@falseFlag),
('Peter Pan','10693',24532000,1953,@falseFlag,@trueFlag),
('Pinocchio','10895',71846260,1940,@falseFlag,@trueFlag),
('Pirates of the Caribbean: At World''s End','285',961000000,2007,@trueFlag,@falseFlag),
('Pirates of the Caribbean: Dead Man''s Chest','58',1065659812,2006,@trueFlag,@trueFlag),
('Pirates of the Caribbean: The Curse of the Black Pearl','22',655011224,2003,@trueFlag,@falseFlag),
('Quo Vadis?','11620',11901662,1951,@falseFlag,@trueFlag),
('Raiders of the Lost Ark','85',383000000,1981,@trueFlag,@trueFlag),
('Rain Man','380',354825435,1987,@falseFlag,@trueFlag),
('Ratatouille','2062',621325849,2007,@trueFlag,@falseFlag),
('Rocky','1374',300473716,1976,@trueFlag,@trueFlag),
('Rush Hour 2','5175',347325802,2001,@trueFlag,@falseFlag),
('Saving Private Ryan','857',481840909,1998,@trueFlag,@trueFlag),
('Shrek','808',484409218,2001,@trueFlag,@falseFlag),
('Shrek 2','809',920665658,2004,@trueFlag,@trueFlag),
('Shrek the Third','810',798900000,2007,@trueFlag,@falseFlag),
('Signs','2675',408247917,2002,@trueFlag,@falseFlag),
('Snow White and the Seven Dwarfs','408',8000000,1937,@trueFlag,@trueFlag),
('Spider-Man','557',806742000,2002,@trueFlag,@trueFlag),
('Spider-Man 2','558',783766341,2004,@trueFlag,@falseFlag),
('Spider-Man 3','559',806742000,2007,@trueFlag,@trueFlag),
('Star Trek','13475',337434109,2009,@trueFlag,@falseFlag),
('Star Wars Episode I: The Phantom Menace','1893',924317558,1999,@trueFlag,@trueFlag),
('Star Wars Episode II: Attack of the Clones','1894',649398328,2002,@trueFlag,@falseFlag),
('Star Wars Episode III: Revenge of the Sith','1895',850000000,2005,@trueFlag,@trueFlag),
('Star Wars Episode IV: A New Hope','11',775398007,1977,@trueFlag,@falseFlag),
('Star Wars Episode V: The Empire Strikes Back','1891',538400000,1980,@trueFlag,@falseFlag),
('Star Wars Episode VI: Return of the Jedi','1892',572700000,1983,@trueFlag,@trueFlag),
('Superman','1924',391081192,1978,@trueFlag,@falseFlag),
('Superman Returns','1452',391081192,2006,@trueFlag,@falseFlag),
('Terminator 2: Judgment Day','280',520000000,1991,@trueFlag,@trueFlag),
('The Chronicles of Narnia','411',748806957,2005,@trueFlag,@falseFlag),
('The Da Vinci Code','591',755724413,2006,@trueFlag,@falseFlag),
('The Dark Knight','155',1001921825,2008,@trueFlag,@trueFlag),
('The Godfather','238',245066411,1972,@trueFlag,@trueFlag),
('The Hangover','18785',164717015,2009,@trueFlag,@falseFlag),
('The Lord of the Rings: The Fellowship of the Ring','120',871368364,2001,@trueFlag,@falseFlag),
('The Lord of the Rings: The Return of the King','122',1118888979,2003,@trueFlag,@trueFlag),
('The Lord of the Rings: The Two Towers','121',926287400,2002,@trueFlag,@falseFlag),
('The Lost World: Jurassic Park','330',786686679,1997,@trueFlag,@falseFlag),
('The Matrix Reloaded','604',738599701,2003,@trueFlag,@falseFlag),
('The Sixth Sense','745',672806292,1999,@trueFlag,@falseFlag),
('The Ten Commandments','6844',65000000,1956,@trueFlag,@trueFlag),
('Top Gun','744',79400000,1986,@falseFlag,@trueFlag),
('The Towering Inferno','5919',116000000,1974,@trueFlag,@trueFlag),
('Three Men and a Baby','12154',81313000,1987,@falseFlag,@trueFlag),
('Thunderball','660',141195658,1965,@trueFlag,@trueFlag),
('Titanic','597',1845034188,1997,@trueFlag,@trueFlag),
('Tootsie','9576',177200000,1982,@trueFlag,@falseFlag),
('Toy Story','862',361958736,1995,@falseFlag,@trueFlag),
('Toy Story 2','863',485015179,1999,@trueFlag,@falseFlag),
('Transformers','1858',831268381,2007,@trueFlag,@falseFlag),
('Transformers: Revenge of the Fallen','8373',831268381,2009,@trueFlag,@trueFlag),
('Twister','664',494471524,1996,@trueFlag,@falseFlag),
('Up','14160',1118888979,2009,@trueFlag,@falseFlag),
('WALL-E','10681',495383652,2008,@trueFlag,@falseFlag),
('War of the Worlds','74',591739379,2005,@trueFlag,@falseFlag),
('Wedding Crashers','9522',285176741,2005,@trueFlag,@falseFlag),
('X2','740',407557613,2003,@trueFlag,@falseFlag),
('X-Men: The Last Stand','741',458751448,2000,@trueFlag,@falseFlag);

DELETE FROM [mp].[YearTopGrossingMovie];
DELETE FROM [mp].[Movie];
DELETE FROM [mp].[MovieExternalIdentifier];

INSERT INTO [mp].[MovieExternalIdentifier]
(
    [ExternalIdentifierValue]
)
SELECT DISTINCT [buffer].[TheMovieDBIdentifier]    
FROM @buffer AS [buffer];

INSERT INTO [mp].[Movie]
(
    [MovieExternalIdentifierKey],
	[Title],
	[TopGrossing],
	[WorldwideRevenue],
	[Year]
)
SELECT DISTINCT 
    [external].[MovieExternalIdentifierKey],
    [buffer].[Title],
    [buffer].[TopGrossing],
    [buffer].[WorldwideRevenue],
    [buffer].[Year]
FROM @buffer AS [buffer]
INNER JOIN [mp].[MovieExternalIdentifier] AS [external]
    ON [buffer].[TheMovieDBIdentifier] = [external].[ExternalIdentifierValue];

INSERT INTO [mp].[YearTopGrossingMovie]
(
	[MovieKey]
)
SELECT DISTINCT [m].[MovieKey] 
FROM [mp].[Movie] AS [m]
INNER JOIN @buffer AS [b]
    ON [m].[Title] = [b].[Title]
WHERE
        [b].[TopGrossOfYear] = @trueFlag;

GO

GO

GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        DECLARE @VarDecimalSupported AS BIT;
        SELECT @VarDecimalSupported = 0;
        IF ((ServerProperty(N'EngineEdition') = 3)
            AND (((@@microsoftversion / power(2, 24) = 9)
                  AND (@@microsoftversion & 0xffff >= 3024))
                 OR ((@@microsoftversion / power(2, 24) = 10)
                     AND (@@microsoftversion & 0xffff >= 1600))))
            SELECT @VarDecimalSupported = 1;
        IF (@VarDecimalSupported > 0)
            BEGIN
                EXECUTE sp_db_vardecimal_storage_format N'$(DatabaseName)', 'ON';
            END
    END


GO
