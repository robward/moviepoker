﻿namespace MoviePoker.Data.LocalDataAccess
{
    using System.Globalization;
    using System.Linq;
    using MoviePoker.Data.LocalDataAccess.Properties;

    public partial class Movie
    {
        bool loaded;
        bool? populated;

        public bool Populated
        {
            get
            {
                if (!(this.populated.HasValue))
                {
                    this.Load();

                    lock (this)
                    {
                        this.populated =
                            (
                                (this.MovieExternalIdentifier.MovieActorPersonExternalIdentifier.Count() > 0) ||
                                (this.MovieExternalIdentifier.MovieDirectorPersonExternalIdentifier.Count() > 0)
                            );
                    }
                }
                return this.populated.Value;
            }
        }

        public void Load()
        {
            if (this.loaded)
            {
                return;
            }

            lock (this)
            {
                if (!(this.loaded))
                {
                    if (!(this.MovieExternalIdentifierReference.IsLoaded))
                    {
                        this.MovieExternalIdentifierReference.Load();
                    }

                    if (!(this.MovieExternalIdentifier.MovieActorPersonExternalIdentifier.IsLoaded))
                    {
                        this.MovieExternalIdentifier.MovieActorPersonExternalIdentifier.Load();
                    }

                    if (!(this.MovieExternalIdentifier.MovieDirectorPersonExternalIdentifier.IsLoaded))
                    {
                        this.MovieExternalIdentifier.MovieDirectorPersonExternalIdentifier.Load();
                    }

                    if
                    (
                        (this.YearTopGrossingMovieReference != null) &&
                        (!(this.YearTopGrossingMovieReference.IsLoaded))
                    )
                    {
                        this.YearTopGrossingMovieReference.Load();
                    }

                    this.loaded = true;
                }
            }
        }

        public override string ToString()
        {
            return string.Format(
                CultureInfo.CurrentCulture, 
                Resources.MovieStringTemplate, 
                MoviePokerEntities.FormatKeyString(this.MovieKey));
        }
    }
}
