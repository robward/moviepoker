﻿namespace MoviePoker.Data.LocalDataAccess
{
    using System.Transactions;

    public static class Utility
    {
        public static TransactionScope InitializeTransaction()
        {
            return new TransactionScope(
                TransactionScopeOption.Required, 
                new TransactionOptions(){IsolationLevel = IsolationLevel.Snapshot});
        }
    }
}
