﻿namespace MoviePoker.Data.LocalDataAccess
{
    using System.Globalization;
    using MoviePoker.Data.LocalDataAccess.Properties;

    public partial class YearTopGrossingMovie
    {
        bool loaded;
        
        public bool Populated
        {
            get
            {
                this.Load();
                return this.Movie.Populated;
            }
        }

        public void Load()
        {
            if (this.loaded)
            {
                return;
            }

            lock (this)
            {
                if (!(this.loaded))
                {
                    if (!(this.MovieReference.IsLoaded))
                    {
                        this.MovieReference.Load();
                    }

                    this.Movie.Load();

                    this.loaded = true;
                }
            }
        }

        public override string ToString()
        {
            return string.Format(
                CultureInfo.CurrentCulture, 
                Resources.YearTopGrossingMovieTemplate, 
                MoviePokerEntities.FormatKeyString(this.MovieKey));
        }
    }
}
