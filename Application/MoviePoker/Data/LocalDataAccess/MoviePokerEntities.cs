﻿namespace MoviePoker.Data.LocalDataAccess
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Transactions;
    using MoviePoker.Data.LocalDataAccess.Properties;
    using MoviePoker.Management;

    public partial class MoviePokerEntities
    {
        void AddCardToDeck(Deck deck, Card card)
        {
            Tracing.TraceInformation(Resources.AddingCardToDeckTrace, deck, card);

            DeckCard deckCard = new DeckCard();
            deckCard.Card = card;
            deckCard.Deck = deck;
            deckCard.Index = (byte)deck.DeckCard.Count;
            this.AddToDeckCard(deckCard);

            this.SaveChanges(true);

            Tracing.TraceInformation(Resources.AddedCardToDeckTrace, deck, card);
        }

        public Card AddMovieCardToDeck(Deck deck, MovieExternalIdentifier identifier)
        {
            var movieQuery =
                    from movieItem in this.Movie
                    where movieItem.MovieExternalIdentifier.MovieExternalIdentifierKey == identifier.MovieExternalIdentifierKey
                    select movieItem;
            Movie movie = movieQuery.FirstOrDefault();
            if (null == movie)
            {
                throw new BadIdentifierException(identifier.ExternalIdentifierValue);
            }
            return this.AddMovieCardToDeck(deck, movie);
        }

        public Card AddMovieCardToDeck(Deck deck, Movie movie)
        {
            using (TransactionScope transaction = Utility.InitializeTransaction())
            {
                var cardQuery =
                    from cardItem in this.Card
                    from movieItem in cardItem.Movie
                    where movieItem.MovieKey == movie.MovieKey
                    select cardItem;
                Card result = cardQuery.FirstOrDefault();

                if (null == result)
                {
                    result = new Card();
                    result.Movie.Add(movie);
                    this.AddToCard(result);
                }

                this.AddCardToDeck(deck, result);

                transaction.Complete();

                return result;
            }
        }

       public Card AddPersonCardToDeck(Deck deck, Person person)
       {
            using (TransactionScope transaction = Utility.InitializeTransaction())
            {
                var cardQuery =
                    from cardItem in this.Card
                    from personItem in cardItem.Person
                    where personItem.PersonKey == person.PersonKey
                    select cardItem;
                Card result = cardQuery.FirstOrDefault();

                if (null == result)
                {
                    result = new Card();
                    result.Person.Add(person);
                    this.AddToCard(result);
                }

                this.AddCardToDeck(deck, result);

                this.SaveChanges(true);
                transaction.Complete();

                return result;
            }
        }

        public Deck AddToDeck()
        {
            Deck result = new Deck();
            this.AddToDeck(result);
            return result;
        }

        public Movie AddToMovie(
            string title,
            string externalIdentifier,
            short year,
            long worldwideRevenue)
        {
            using (TransactionScope transaction = Utility.InitializeTransaction())
            {
                Movie result = new Movie();
                result.MovieExternalIdentifier = this.GetMovieExternalIdentifier(externalIdentifier);
                result.Title = title;
                result.Year = year;
                result.WorldwideRevenue = worldwideRevenue;
                this.AddToMovie(result);

                this.SaveChanges(true);
                transaction.Complete();

                return result;
            }
        }

        public MovieExternalIdentifier AddToMovieExternalIdentifier(string externalIdentifier)
        {
            MovieExternalIdentifier result = new MovieExternalIdentifier();
            result.ExternalIdentifierValue = externalIdentifier;
            this.AddToMovieExternalIdentifier(result);
            return result;
        }

        public Person AddToPerson(string name, string externalIdentifier)
        {
            using (TransactionScope transaction = Utility.InitializeTransaction())
            {
                Person result = new Person();
                result.PersonExternalIdentifier = this.GetPersonExternalIdentifier(externalIdentifier);
                result.Name = name;
                this.AddToPerson(result);

                this.SaveChanges(true);
                transaction.Complete();

                return result;
            }
        }

        public PersonExternalIdentifier AddToPersonExternalIdentifier(string externalIdentifier)
        {
            PersonExternalIdentifier result = new PersonExternalIdentifier();
            result.ExternalIdentifierValue = externalIdentifier;
            this.AddToPersonExternalIdentifier(result);
            return result;
        }

        public Card AddYearCardToDeck(Deck deck, YearTopGrossingMovie movie)
        {
            using (TransactionScope transaction = Utility.InitializeTransaction())
            {
                var cardQuery =
                    from cardItem in this.Card
                    from yearTopGrossingMovieItem in cardItem.YearTopGrossingMovie
                    where yearTopGrossingMovieItem.MovieKey == movie.MovieKey
                    select cardItem;
                Card result = cardQuery.FirstOrDefault();
                
                if (null == result)
                {
                    result = new Card();
                    result.YearTopGrossingMovie.Add(movie);
                    this.AddToCard(result);
                }

                this.AddCardToDeck(deck, result);

                this.SaveChanges(true);
                transaction.Complete();

                return result;
            }
        }

        internal static string FormatKeyString(long key)
        {
            return string.Format(CultureInfo.CurrentCulture, Resources.KeyStringTemplate, key);
        }

        public MovieExternalIdentifier GetMovieExternalIdentifier(string externalIdentifierValue)
        {
            var externalIdentifierQuery =
                from externalIdentifierItem in this.MovieExternalIdentifier
                where
                        externalIdentifierItem.ExternalIdentifierValue.Equals(externalIdentifierValue, StringComparison.OrdinalIgnoreCase)
                select externalIdentifierItem;
            MovieExternalIdentifier result = externalIdentifierQuery.FirstOrDefault();
            if (result != null)
            {
                return result;
            }
            return this.AddToMovieExternalIdentifier(externalIdentifierValue);
        }

        public PersonExternalIdentifier GetPersonExternalIdentifier(string externalIdentifierValue)
        {
            var externalIdentifierQuery =
                from externalIdentifierItem in this.PersonExternalIdentifier
                where
                        externalIdentifierItem.ExternalIdentifierValue.Equals(externalIdentifierValue, StringComparison.OrdinalIgnoreCase)
                select externalIdentifierItem;
            PersonExternalIdentifier result = externalIdentifierQuery.FirstOrDefault();
            if (result != null)
            {
                return result;
            }
            return this.AddToPersonExternalIdentifier(externalIdentifierValue);
        }

        public bool TryRandomlySelectMovieByDifficulty(int difficultyQuotient, long? deckKey, out Movie movie)
        {
            movie = this.RandomlySelectMovieByDifficulty(difficultyQuotient, deckKey).FirstOrDefault();
            if(movie != null)
            {
                movie.Load();
                return true;
            }

            return false;
        }
    }
}
