﻿namespace MoviePoker.Data.LocalDataAccess
{
    using System.Globalization;
    using MoviePoker.Data.LocalDataAccess.Properties;

    public partial class Deck
    {
        bool loaded;

        public void Load()
        {
            if (this.loaded)
            {
                return;
            }

            lock (this)
            {
                if (!(this.loaded))
                {
                    if (!(this.DeckCard.IsLoaded))
                    {
                        this.DeckCard.Load();
                    }

                    foreach (DeckCard deckCard in this.DeckCard)
                    {
                        if (!(deckCard.CardReference.IsLoaded))
                        {
                            deckCard.CardReference.Load();
                        }

                        deckCard.Card.Load();
                    }

                    this.loaded = true;
                }
            }
        }

        public override string ToString()
        {
            return string.Format(
                CultureInfo.CurrentCulture, 
                Resources.DeckStringTemplate, 
                MoviePokerEntities.FormatKeyString(this.DeckKey), 
                this.DeckCard.Count);
        }
    }
}
