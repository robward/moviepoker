﻿namespace MoviePoker.Data.LocalDataAccess
{
    using System.Globalization;
    using System.Linq;
    using MoviePoker.Data.LocalDataAccess.Properties;

    public partial class Card
    {
        bool loaded;

        public void Load()
        {
            if (this.loaded)
            {
                return;
            }

            lock (this)
            {
                if (!(this.loaded))
                {
                    if (this.Movie != null)
                    {
                        this.Movie.Load();
                    }

                    if (this.Person != null)
                    {
                        this.Person.Load();
                    }

                    if (this.YearTopGrossingMovie != null)
                    {
                        this.YearTopGrossingMovie.Load();
                    }

                    this.loaded = true;
                }
            }
        }

        public override string ToString()
        {
            string cardType = null;
            
            object detail = null;

            detail = this.Movie.FirstOrDefault();
            if (detail != null)
            {
                cardType = Resources.MovieString;
            }
            else
            {
                detail = this.Person.FirstOrDefault();
                if(detail != null)
                {
                    cardType = Resources.PersonString;
                }
                else
                {
                    detail = this.YearTopGrossingMovie.FirstOrDefault();
                    if(detail != null)
                    {
                        cardType = Resources.YearString;
                    }
                }
            }
            
            return string.Format(
                CultureInfo.CurrentCulture, 
                Resources.CardStringTemplate, 
                cardType,
                MoviePokerEntities.FormatKeyString(this.CardKey), 
                detail);
        }
    }
}
