﻿namespace MoviePoker.Data.LocalDataAccess
{
    using System.Linq;

    public partial class DeckCard
    {
        bool loaded;
        
        public void Load()
        {
            if (this.loaded)
            {
                return;
            }

            lock (this)
            {
                if (!(this.loaded))
                {
                    this.CardReference.Load();
                    this.Card.Movie.Load();

                    this.loaded = true;
                }
            }
        }
    }
}
