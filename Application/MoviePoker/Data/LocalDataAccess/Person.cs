﻿namespace MoviePoker.Data.LocalDataAccess
{
    using System.Globalization;
    using System.Linq;
    using MoviePoker.Data.LocalDataAccess.Properties;

    public partial class Person
    {
        bool loaded;
        bool? populated;

        public bool Populated
        {
            get
            {
                if (!(this.populated.HasValue))
                {
                    this.Load();

                    lock (this)
                    {
                        this.populated =
                            (
                                (this.PersonExternalIdentifier.ActingCreditMovieExternalIdentifier.Count() > 0) ||
                                (this.PersonExternalIdentifier.DirectingCreditMovieExternalIdentifier.Count() > 0)
                            );
                    }
                }
                return this.populated.Value;
            }
        }

        public void Load()
        {
            if (this.loaded)
            {
                return;
            }

            lock (this)
            {
                if (!(this.loaded))
                {
                    if (!(this.PersonExternalIdentifierReference.IsLoaded))
                    {
                        this.PersonExternalIdentifierReference.Load();
                    }

                    if (!(this.PersonExternalIdentifier.ActingCreditMovieExternalIdentifier.IsLoaded))
                    {
                        this.PersonExternalIdentifier.ActingCreditMovieExternalIdentifier.Load();
                    }

                    if (!(this.PersonExternalIdentifier.DirectingCreditMovieExternalIdentifier.IsLoaded))
                    {
                        this.PersonExternalIdentifier.DirectingCreditMovieExternalIdentifier.Load();
                    }

                    this.loaded = true;
                }
            }
        }

        public override string ToString()
        {
            return string.Format(
                CultureInfo.CurrentCulture, 
                Resources.PersonStringTemplate, 
                MoviePokerEntities.FormatKeyString(this.PersonKey));
        }
    }
}
