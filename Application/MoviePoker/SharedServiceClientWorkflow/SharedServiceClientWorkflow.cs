﻿namespace MoviePoker.Workflow
{
    using System;
    using System.Collections.Generic;
    using System.Workflow.Activities;

    public sealed partial class SharedServiceClientWorkflow : SequentialWorkflowActivity
    {
        static TimeSpan defaultDelay = new TimeSpan(0, 0, 10);

        bool currentInvocationSucceeded;
        string currentInvocationIdentifier;
        TimeSpan delay = default(TimeSpan);
        Queue<string> invocationQueue = new Queue<string>();

        public SharedServiceClientWorkflow()
        {
            InitializeComponent();
        }

        public string CurrentInvocationIdentifier
        {
            get
            {
                return this.currentInvocationIdentifier;
            }
        }

        public bool CurrentInvocationSucceeded
        {
            get
            {
                return this.currentInvocationSucceeded;
            }

            set
            {
                this.currentInvocationSucceeded = value;
            }
        }

        internal Queue<string> InvocationQueue
        {
            get
            {
                return this.invocationQueue;
            }
        }

        void OnBeforeCallingDoInvocation(object sender, EventArgs e)
        {
            this.currentInvocationIdentifier = this.invocationQueue.Peek();
        }

        void OnBeforeDelay(object sender, EventArgs e)
        {
            this.delayActivity.TimeoutDuration = this.delay;
        }

        private void OnInput(object sender, ExternalDataEventArgs e)
        {
            SharedServiceClientEventArgs arguments = e as SharedServiceClientEventArgs;
            if (null == arguments)
            {
                return;
            }

            this.invocationQueue.Enqueue(arguments.InvocationIdentifier);
        }

        void OnInvocationFailed(object sender, EventArgs e)
        {
            this.delay += SharedServiceClientWorkflow.defaultDelay;
        }

        void OnInvocationSucceeded(object sender, EventArgs e)
        {
            this.invocationQueue.Dequeue();
            this.delay = SharedServiceClientWorkflow.defaultDelay;
        }
    }
}
