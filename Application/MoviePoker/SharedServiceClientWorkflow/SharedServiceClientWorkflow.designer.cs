﻿namespace MoviePoker.Workflow
{
    using System;
    using System.ComponentModel;
    using System.ComponentModel.Design;
    using System.Collections;
    using System.Drawing;
    using System.Reflection;
    using System.Workflow.ComponentModel.Compiler;
    using System.Workflow.ComponentModel.Serialization;
    using System.Workflow.ComponentModel;
    using System.Workflow.ComponentModel.Design;
    using System.Workflow.Runtime;
    using System.Workflow.Activities;
    using System.Workflow.Activities.Rules;

    partial class SharedServiceClientWorkflow
    {
        #region Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCode]
        private void InitializeComponent()
        {
            this.CanModifyActivities = true;
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference1 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.ComponentModel.ActivityBind activitybind1 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding1 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind2 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding2 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference2 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference3 = new System.Workflow.Activities.Rules.RuleConditionReference();
            this.failedInvocationActivity = new System.Workflow.Activities.CodeActivity();
            this.successfulInvocationActivity = new System.Workflow.Activities.CodeActivity();
            this.invocationFailedActivity = new System.Workflow.Activities.IfElseBranchActivity();
            this.invocationSucceededActivity = new System.Workflow.Activities.IfElseBranchActivity();
            this.delayActivity = new System.Workflow.Activities.DelayActivity();
            this.ifInvocationSucceededActivity = new System.Workflow.Activities.IfElseActivity();
            this.callDoInvocationActivity = new System.Workflow.Activities.CallExternalMethodActivity();
            this.workLoopSequenceActivity = new System.Workflow.Activities.SequenceActivity();
            this.waitForInputActivity = new System.Workflow.Activities.HandleExternalEventActivity();
            this.workLoopActivity = new System.Workflow.Activities.WhileActivity();
            this.inputLoopSequenceActivity = new System.Workflow.Activities.SequenceActivity();
            this.inputLoopActivity = new System.Workflow.Activities.WhileActivity();
            // 
            // failedInvocationActivity
            // 
            this.failedInvocationActivity.Name = "failedInvocationActivity";
            this.failedInvocationActivity.ExecuteCode += new System.EventHandler(this.OnInvocationFailed);
            // 
            // successfulInvocationActivity
            // 
            this.successfulInvocationActivity.Name = "successfulInvocationActivity";
            this.successfulInvocationActivity.ExecuteCode += new System.EventHandler(this.OnInvocationSucceeded);
            // 
            // invocationFailedActivity
            // 
            this.invocationFailedActivity.Activities.Add(this.failedInvocationActivity);
            this.invocationFailedActivity.Name = "invocationFailedActivity";
            // 
            // invocationSucceededActivity
            // 
            this.invocationSucceededActivity.Activities.Add(this.successfulInvocationActivity);
            ruleconditionreference1.ConditionName = "invocationSucceededCondition";
            this.invocationSucceededActivity.Condition = ruleconditionreference1;
            this.invocationSucceededActivity.Name = "invocationSucceededActivity";
            // 
            // delayActivity
            // 
            this.delayActivity.Name = "delayActivity";
            this.delayActivity.TimeoutDuration = System.TimeSpan.Parse("00:00:00");
            this.delayActivity.InitializeTimeoutDuration += new System.EventHandler(this.OnBeforeDelay);
            // 
            // ifInvocationSucceededActivity
            // 
            this.ifInvocationSucceededActivity.Activities.Add(this.invocationSucceededActivity);
            this.ifInvocationSucceededActivity.Activities.Add(this.invocationFailedActivity);
            this.ifInvocationSucceededActivity.Name = "ifInvocationSucceededActivity";
            // 
            // callDoInvocationActivity
            // 
            this.callDoInvocationActivity.InterfaceType = typeof(MoviePoker.Workflow.ISharedServiceClientExternalDataExchangeService);
            this.callDoInvocationActivity.MethodName = "DoInvocation";
            this.callDoInvocationActivity.Name = "callDoInvocationActivity";
            activitybind1.Name = "SharedServiceClientWorkflow";
            activitybind1.Path = "CurrentInvocationIdentifier";
            workflowparameterbinding1.ParameterName = "invocationIdentifier";
            workflowparameterbinding1.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind1)));
            activitybind2.Name = "SharedServiceClientWorkflow";
            activitybind2.Path = "CurrentInvocationSucceeded";
            workflowparameterbinding2.ParameterName = "(ReturnValue)";
            workflowparameterbinding2.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind2)));
            this.callDoInvocationActivity.ParameterBindings.Add(workflowparameterbinding1);
            this.callDoInvocationActivity.ParameterBindings.Add(workflowparameterbinding2);
            this.callDoInvocationActivity.MethodInvoking += new System.EventHandler(this.OnBeforeCallingDoInvocation);
            // 
            // workLoopSequenceActivity
            // 
            this.workLoopSequenceActivity.Activities.Add(this.callDoInvocationActivity);
            this.workLoopSequenceActivity.Activities.Add(this.ifInvocationSucceededActivity);
            this.workLoopSequenceActivity.Activities.Add(this.delayActivity);
            this.workLoopSequenceActivity.Name = "workLoopSequenceActivity";
            // 
            // waitForInputActivity
            // 
            this.waitForInputActivity.EventName = "QueueInvocation";
            this.waitForInputActivity.InterfaceType = typeof(MoviePoker.Workflow.ISharedServiceClientExternalDataExchangeService);
            this.waitForInputActivity.Name = "waitForInputActivity";
            this.waitForInputActivity.Invoked += new System.EventHandler<System.Workflow.Activities.ExternalDataEventArgs>(this.OnInput);
            // 
            // workLoopActivity
            // 
            this.workLoopActivity.Activities.Add(this.workLoopSequenceActivity);
            ruleconditionreference2.ConditionName = "workLoopActivityCondition";
            this.workLoopActivity.Condition = ruleconditionreference2;
            this.workLoopActivity.Name = "workLoopActivity";
            // 
            // inputLoopSequenceActivity
            // 
            this.inputLoopSequenceActivity.Activities.Add(this.workLoopActivity);
            this.inputLoopSequenceActivity.Activities.Add(this.waitForInputActivity);
            this.inputLoopSequenceActivity.Name = "inputLoopSequenceActivity";
            // 
            // inputLoopActivity
            // 
            this.inputLoopActivity.Activities.Add(this.inputLoopSequenceActivity);
            ruleconditionreference3.ConditionName = "inputLookActivityCondition";
            this.inputLoopActivity.Condition = ruleconditionreference3;
            this.inputLoopActivity.Name = "inputLoopActivity";
            // 
            // SharedServiceClientWorkflow
            // 
            this.Activities.Add(this.inputLoopActivity);
            this.Name = "SharedServiceClientWorkflow";
            this.CanModifyActivities = false;

        }

        #endregion

        private HandleExternalEventActivity waitForInputActivity;
        private SequenceActivity inputLoopSequenceActivity;
        private CodeActivity failedInvocationActivity;
        private DelayActivity delayActivity;
        private IfElseBranchActivity invocationFailedActivity;
        private IfElseBranchActivity invocationSucceededActivity;
        private IfElseActivity ifInvocationSucceededActivity;
        private SequenceActivity workLoopSequenceActivity;
        private CodeActivity successfulInvocationActivity;
        private WhileActivity workLoopActivity;
        private CallExternalMethodActivity callDoInvocationActivity;
        private WhileActivity inputLoopActivity;






































    }
}
