﻿namespace MoviePoker.Workflow
{
    using System;
    using System.Workflow.Activities;
    
    [Serializable]
    public class SharedServiceClientEventArgs : ExternalDataEventArgs
    {
        private string invocationIdentifier;

        public SharedServiceClientEventArgs(Guid instanceIdentifier, string invocationIdentifier)
            : base(instanceIdentifier)
        {
            this.invocationIdentifier = invocationIdentifier;
        }

        public string InvocationIdentifier
        {
            get
            {
                return this.invocationIdentifier;
            }
        }
    }
}
