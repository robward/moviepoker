﻿namespace MoviePoker.Workflow
{
    using System;
    using System.Workflow.Activities;

	[ExternalDataExchange]
    public interface ISharedServiceClientExternalDataExchangeService
    {
        event EventHandler<SharedServiceClientEventArgs> QueueInvocation;

        bool DoInvocation(string invocationIdentifier);
    }
}
