﻿namespace MoviePoker.Workflow
{
    public interface ISharedServiceClient
    {
        bool DoInvocation(string invocationIdentifier);
    }
}
