﻿namespace MoviePoker.Workflow
{
    using System;
    using System.Workflow.Activities;
    using System.Workflow.Runtime;

	public sealed class SharedServiceClientWorkflowHost: IDisposable
	{
        SharedServiceClientExternalDataExchangeServiceImplementation sharedServiceClientExternalDataExchangeService;
        Guid workflowInstanceIdentifier;
        WorkflowRuntime workflowRuntime;
        
        public SharedServiceClientWorkflowHost(ISharedServiceClient sharedServiceClient)
        {
            this.workflowRuntime = new WorkflowRuntime();

            this.sharedServiceClientExternalDataExchangeService =
                new SharedServiceClientExternalDataExchangeServiceImplementation(sharedServiceClient);
            ExternalDataExchangeService externalDataExchangeService = new ExternalDataExchangeService();
            workflowRuntime.AddService(externalDataExchangeService);
            externalDataExchangeService.AddService(this.sharedServiceClientExternalDataExchangeService);

            workflowRuntime.StartRuntime();
            WorkflowInstance workflowInstance = workflowRuntime.CreateWorkflow(typeof(SharedServiceClientWorkflow));
            this.workflowInstanceIdentifier = workflowInstance.InstanceId;
            workflowInstance.Start();
        }

        public void Dispose()
        {
            workflowRuntime.StopRuntime();
            this.workflowRuntime.Dispose();
        }

        public void QueueInvocation(string invocationIdentifier)
        {
            this.sharedServiceClientExternalDataExchangeService.RaiseInvocationEvent(
                this.workflowInstanceIdentifier, 
                invocationIdentifier);
        }

        class SharedServiceClientExternalDataExchangeServiceImplementation : ISharedServiceClientExternalDataExchangeService
        {
            ISharedServiceClient client;

            internal SharedServiceClientExternalDataExchangeServiceImplementation(ISharedServiceClient client)
            {
                this.client = client;
            }

            public event EventHandler<SharedServiceClientEventArgs> QueueInvocation;

            public bool DoInvocation(string invocationIdentifier)
            {
                return this.client.DoInvocation(invocationIdentifier);
            }

            internal void RaiseInvocationEvent(Guid workflowInstanceIdentifier, string invocationIdentifier)
            {
                EventHandler<SharedServiceClientEventArgs> invocation = this.QueueInvocation;
                if (invocation != null)
                {
                    invocation(null,new SharedServiceClientEventArgs(workflowInstanceIdentifier, invocationIdentifier));
                }
            }
        }
    }
}
