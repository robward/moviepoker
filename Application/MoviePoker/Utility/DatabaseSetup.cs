﻿namespace MoviePoker.Utility
{
    using Microsoft.Data.Schema.UnitTesting;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass()]
    public class DatabaseSetup
    {
        [AssemblyInitialize()]
        public static void IntializeAssembly(TestContext ctx)
        {
            //   Setup the test database based on setting in the
            // configuration file
            DatabaseTestClass.TestService.DeployDatabaseProject();
            DatabaseTestClass.TestService.GenerateData();
        }

    }
}
