﻿namespace MoviePoker.Utility
{
    using System;
    using System.Globalization;
    using System.Reflection;
    using System.Threading;
    using MoviePoker.Management;
    using MoviePoker.Utility.Properties;

    public class AsynchronousResult : IAsyncResult, IDisposable
    {
        const int CompletionPendingFlag = 0;
        const int CompletedSynchronouslyFlag = 1;
        const int CompletedAsynchronouslyFlag = 2;

        readonly AsyncCallback asynchronousCallbackField;
        readonly object asynchronousStateField;

        int completionStateField;
        object dataField;
        object locker = new object();
        ManualResetEvent waitHandleField;
        Exception exceptionField;

        public AsynchronousResult(
            AsyncCallback asynchronousCallback,
            object state)
        {
            this.asynchronousCallbackField = asynchronousCallback;
            this.asynchronousStateField = state;
        }

        public object AsyncState
        {
            get { return this.asynchronousStateField; }
        }

        public bool CompletedSynchronously
        {
            get { return (AsynchronousResult.CompletedSynchronouslyFlag == this.completionStateField); }
        }

        public object Data
        {
            get
            {
                return this.dataField;
            }

            set
            {
                this.dataField = value;
            }
        }

        public WaitHandle AsyncWaitHandle
        {
            get
            {
                lock (this.locker)
                {
                    if (null == this.waitHandleField)
                    {
                        this.waitHandleField = new ManualResetEvent(this.IsCompleted);
                    }

                    return this.waitHandleField;
                }
            }
        }

        public bool IsCompleted
        {
            get
            {
                lock (this.locker)
                {
                    return (this.completionStateField != AsynchronousResult.CompletionPendingFlag);
                }
            }
        }

        public void Complete()
        {
            this.Complete(null);
        }

        public void Complete(object result)
        {
            this.Complete(result, false);
        }

        public void Complete(object result, bool completedSynchronously)
        {
            Exception exception = result as Exception;
            object data = null;
            if(null == exception)
            {
                data = result;
            }
            this.Complete(data, exception, completedSynchronously);
        }

        void Complete(object data, Exception exception, bool completedSynchronously)
        {
            if (data != null)
            {
                this.dataField = data;
            }

            if (null != exception)
            {
                Tracing.TraceError(
                    string.Format(
                        CultureInfo.CurrentCulture,
                        Resources.AsynchronousOperationError,
                        exception));
            }

            this.exceptionField = exception;

            lock (this.locker)
            {
                if (this.IsCompleted)
                {
                    throw new ManagedException(new InvalidOperationException(Resources.AsynchronousOperationAlreadyCompleted));
                }

                this.completionStateField =
                    completedSynchronously ?
                        AsynchronousResult.CompletedSynchronouslyFlag :
                        AsynchronousResult.CompletedAsynchronouslyFlag;

                if (null != this.waitHandleField)
                {
                    this.waitHandleField.Set();
                }
            }

            if (null != asynchronousCallbackField)
            {
                asynchronousCallbackField(this);
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposeManagedResources)
        {
            if (disposeManagedResources)
            {
                lock (this.locker)
                {
                    if (this.waitHandleField != null)
                    {
                        this.waitHandleField.Close();
                    }
                }
            }
        }

        public object EndInvoke()
        {
            if (!(this.IsCompleted))
            {
                this.AsyncWaitHandle.WaitOne();
                this.AsyncWaitHandle.Close();
                this.waitHandleField = null;
            }

            if (null != this.exceptionField)
            {
                if ((this.exceptionField is TargetInvocationException) && (this.exceptionField.InnerException != null))
                {
                    throw this.exceptionField.InnerException;
                }
                else
                {
                    throw this.exceptionField;
                }
            }

            return this.dataField;
        }
    }
}
