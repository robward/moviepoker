﻿namespace  MoviePoker.Test.Game
{
    using System.Diagnostics;
    using System.Globalization;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using MoviePoker;
    
    /// <summary>
    ///  Tests the interface of the Game library
    /// </summary>
    [TestClass]
    public class Test
    {
        const byte RoundsOfPlayLimit = 5;

        byte roundsOfPlayCount;
        TestContext testContextInstance;
        
        public Test()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestCreatingAverageDeck()
        {
            GameDeck deck = new GameDeck(Difficulty.Average);
            deck.AddedCard += new System.EventHandler<AddedCardEventArgs>(deck_AddedCard);
            Assert.AreEqual(deck.ShuffledCards.Count, 52);
        }

        [TestMethod]
        public void TestCreatingEasyDeck()
        {
            GameDeck deck = new GameDeck(Difficulty.Easy);
            deck.AddedCard += new System.EventHandler<AddedCardEventArgs>(deck_AddedCard);
            Assert.AreEqual(deck.ShuffledCards.Count, 52);
        }

        [TestMethod]
        public void TestCreatingHardDeck()
        {
            GameDeck deck = new GameDeck(Difficulty.Hard);
            deck.AddedCard += new System.EventHandler<AddedCardEventArgs>(deck_AddedCard);
            Assert.AreEqual(deck.ShuffledCards.Count, 52);
        }
 
        [TestMethod]
        public void TestPlay()
        {
            Game game = new Game();
            game.NewRound += new System.EventHandler<NewRoundEventArgs>(game_NewRound);
            game.Play();
        }

        void game_NewRound(object sender, NewRoundEventArgs arguments)
        {
            this.roundsOfPlayCount++;
            if ((Test.RoundsOfPlayLimit - 1) == this.roundsOfPlayCount)
            {
                arguments.Round.Last = true;
            }
        }

        void deck_AddedCard(object sender, AddedCardEventArgs arguments)
        {
            Debug.WriteLine(string.Format(CultureInfo.CurrentCulture, "Added card number {0}.", arguments.CardNumber));
        }
    }
}
