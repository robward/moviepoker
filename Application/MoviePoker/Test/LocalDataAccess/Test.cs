﻿namespace MoviePoker.Test.LocalDataAccess
{
    using System;
    using System.Linq;
    using System.Transactions;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using MoviePoker.Data.LocalDataAccess;

    /// <summary>
    /// Tests the interface of the LocalDataAccess library
    /// </summary>
    [TestClass]
    public class Test
    {
        /*
         * The use of the application configuration file in these tests depends on the post build event, 
         * copy /Y $(ProjectDir)app.config $(TargetPath).config 
         * 
         */

        public Test()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion
        
        [TestMethod]
        public void TestCreatingMovie()
        {
            string title = Guid.NewGuid().ToString();

            using (MoviePokerEntities context = new MoviePokerEntities())
            {
                MovieExternalIdentifier movieIdentifier = new MovieExternalIdentifier();
                movieIdentifier.ExternalIdentifierValue = Guid.NewGuid().ToString();
                context.AddToMovieExternalIdentifier(movieIdentifier);
                context.SaveChanges();

                Movie movie = new Movie();
                movie.MovieExternalIdentifier = movieIdentifier;
                movie.Title = title;
                movie.Year = (short)DateTime.Now.Year;
                movie.TopGrossing = false;
                context.AddToMovie(movie);
                context.SaveChanges();

                var movieQuery =
                    from movieItem in context.Movie
                    where movieItem.Title.Equals(title, StringComparison.OrdinalIgnoreCase)
                    select movieItem;
                Assert.IsTrue(1 == movieQuery.Count());

                PersonExternalIdentifier directorIdentifier = new PersonExternalIdentifier();
                directorIdentifier.ExternalIdentifierValue = Guid.NewGuid().ToString();
                context.AddToPersonExternalIdentifier(directorIdentifier);
                context.SaveChanges();

                Person director = new Person();
                director.PersonExternalIdentifier = directorIdentifier;
                director.Name = Guid.NewGuid().ToString();
                context.AddToPerson(director);
                context.SaveChanges();

                PersonExternalIdentifier actorIdentifier = new PersonExternalIdentifier();
                actorIdentifier.ExternalIdentifierValue = Guid.NewGuid().ToString();
                context.AddToPersonExternalIdentifier(actorIdentifier);
                context.SaveChanges();

                Person actor = new Person();
                actor.PersonExternalIdentifier = actorIdentifier;
                actor.Name = Guid.NewGuid().ToString();
                context.AddToPerson(actor);
                context.SaveChanges();

                movie.MovieExternalIdentifier.MovieDirectorPersonExternalIdentifier.Add(directorIdentifier);
                movie.MovieExternalIdentifier.MovieActorPersonExternalIdentifier.Add(actorIdentifier);
                context.SaveChanges();

                movieQuery =
                    from movieItem in context.Movie
                    where movieItem.Title.Equals(title, StringComparison.OrdinalIgnoreCase)
                    select movieItem;
                Assert.IsTrue(1 == movieQuery.Count());

                Movie retrievedMovie = movieQuery.First();
                Assert.IsTrue(1 == retrievedMovie.MovieExternalIdentifier.MovieDirectorPersonExternalIdentifier.Count());
                Assert.IsTrue(1 == retrievedMovie.MovieExternalIdentifier.MovieActorPersonExternalIdentifier.Count());

                movieQuery =
                    from movieItem in context.Movie
                    join movieExternalIdentifierItem in context.MovieExternalIdentifier
                        on movieItem.MovieExternalIdentifier equals movieExternalIdentifierItem
                    from directorExternalIdentifierItem in movieExternalIdentifierItem.MovieDirectorPersonExternalIdentifier
                    where 
                            directorExternalIdentifierItem.ExternalIdentifierValue.Equals(
                                director.PersonExternalIdentifier.ExternalIdentifierValue,
                                StringComparison.OrdinalIgnoreCase)
                    select movieItem;
                Assert.IsTrue(1 == movieQuery.Count());
                retrievedMovie = movieQuery.First();
                Assert.AreEqual<string>(retrievedMovie.Title, title);
                Assert.IsTrue(1 == retrievedMovie.MovieExternalIdentifier.MovieDirectorPersonExternalIdentifier.Count());
                Assert.IsTrue(1 == retrievedMovie.MovieExternalIdentifier.MovieActorPersonExternalIdentifier.Count());

                movieQuery =
                    from movieItem in context.Movie
                    join movieExternalIdentifierItem in context.MovieExternalIdentifier
                        on movieItem.MovieExternalIdentifier equals movieExternalIdentifierItem
                    from actorExternalIdentifierItem in movieExternalIdentifierItem.MovieActorPersonExternalIdentifier
                    where 
                        actorExternalIdentifierItem.ExternalIdentifierValue.Equals(
                            actor.PersonExternalIdentifier.ExternalIdentifierValue,
                            StringComparison.OrdinalIgnoreCase)
                    select movieItem;
                Assert.IsTrue(1 == movieQuery.Count());
                retrievedMovie = movieQuery.First();
                Assert.AreEqual<string>(retrievedMovie.Title, title);
                Assert.IsTrue(1 == retrievedMovie.MovieExternalIdentifier.MovieDirectorPersonExternalIdentifier.Count());
                Assert.IsTrue(1 == retrievedMovie.MovieExternalIdentifier.MovieActorPersonExternalIdentifier.Count());

                movieQuery =
                    from movieItem in context.Movie
                    join movieExternalIdentifierItem in context.MovieExternalIdentifier
                        on movieItem.MovieExternalIdentifier equals movieExternalIdentifierItem
                    from directorExternalIdentifierItem in movieExternalIdentifierItem.MovieDirectorPersonExternalIdentifier
                    from actorExternalIdentifierItem in movieExternalIdentifierItem.MovieActorPersonExternalIdentifier
                    where 
                            directorExternalIdentifierItem.ExternalIdentifierValue.Equals(
                                director.PersonExternalIdentifier.ExternalIdentifierValue,
                                StringComparison.OrdinalIgnoreCase)
                        &&  actorExternalIdentifierItem.ExternalIdentifierValue.Equals(
                            actor.PersonExternalIdentifier.ExternalIdentifierValue,
                            StringComparison.OrdinalIgnoreCase)
                    select movieItem;
                Assert.IsTrue(1 == movieQuery.Count());
                retrievedMovie = movieQuery.First();
                Assert.AreEqual<string>(retrievedMovie.Title, title);
                Assert.IsTrue(1 == retrievedMovie.MovieExternalIdentifier.MovieDirectorPersonExternalIdentifier.Count());
                Assert.IsTrue(1 == retrievedMovie.MovieExternalIdentifier.MovieActorPersonExternalIdentifier.Count());

                var actorQuery =
                    from movieItem in context.Movie
                    join movieExternalIdentifierItem in context.MovieExternalIdentifier
                        on movieItem.MovieExternalIdentifier equals movieExternalIdentifierItem
                    from movieActorExternalIdentifierItem in movieExternalIdentifierItem.MovieActorPersonExternalIdentifier
                    join personExternalIdentifierItem in context.PersonExternalIdentifier
                        on movieActorExternalIdentifierItem equals personExternalIdentifierItem
                    join personItem in context.Person
                        on personExternalIdentifierItem equals personItem.PersonExternalIdentifier
                    where
                            movieItem.Title.Equals(movie.Title, StringComparison.OrdinalIgnoreCase)
                    select personItem;
                Assert.IsTrue(1 == actorQuery.Count());
                Person retrievedPerson = actorQuery.First();
                Assert.AreEqual<long>(retrievedPerson.PersonKey, actor.PersonKey);
                Assert.IsTrue(1 == retrievedPerson.PersonExternalIdentifier.ActingCreditMovieExternalIdentifier.Count);
                Assert.AreEqual<string>(
                    retrievedPerson.PersonExternalIdentifier.ActingCreditMovieExternalIdentifier.First().ExternalIdentifierValue, 
                    movie.MovieExternalIdentifier.ExternalIdentifierValue);
                                 
                context.DeleteObject(director);
                context.DeleteObject(directorIdentifier);
                context.DeleteObject(actor);
                context.DeleteObject(actorIdentifier);
                context.DeleteObject(movie);
                context.DeleteObject(movieIdentifier);
                context.SaveChanges();

                movieQuery =
                    from movieItem in context.Movie
                    where movieItem.Title.Equals(title, StringComparison.OrdinalIgnoreCase)
                    select movieItem;
                Assert.IsTrue(0 == movieQuery.Count());

            }
        }

        [TestMethod]
        public void TestRandomlySelectingMovieByDifficulty()
        {
            using (MoviePokerEntities context = new MoviePokerEntities())
            {
                var query =
                    from item in context.Movie
                    select item;
                Assert.IsTrue(query.Count() == 129);
            }
        }
    }
}
