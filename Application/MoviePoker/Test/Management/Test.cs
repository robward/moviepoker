﻿namespace MoviePoker.Test.Management
{
    using System;
    using System.Diagnostics;
    using System.Threading;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using MoviePoker.Management;

    /// <summary>
    /// Tests the interface of the Management library
    /// </summary>
    [TestClass]
    public class Test
    {
        const string ApplicationLogName = "Application";
        const string ExceptionMessage = "Test exception message";
        AutoResetEvent waitHandle;

        public Test()
        {
        }

        private TestContext testContextInstance;

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        void eventLog_EntryWritten(object sender, EntryWrittenEventArgs eventData)
        {
            if
            (
                (eventData.Entry.Source.Equals(MoviePoker.Management.Properties.Resources.TraceSource,StringComparison.Ordinal))&&
                (eventData.Entry.Message.Equals(Test.ExceptionMessage, StringComparison.Ordinal))
            )
            {
                this.waitHandle.Set();
            }
        }

        [TestMethod]
        public void TestTracingException()
        {
            this.waitHandle = new AutoResetEvent(false);

            EventLog eventLog = new EventLog(Test.ApplicationLogName);
            eventLog.EnableRaisingEvents = true;
            eventLog.EntryWritten += new EntryWrittenEventHandler(eventLog_EntryWritten);
            
            ManagedException exception = new ManagedException(Test.ExceptionMessage);

            Assert.IsTrue(this.waitHandle.WaitOne(new TimeSpan(0,0,30)));
        }        
    }
}
