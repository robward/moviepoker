﻿namespace MoviePoker.Test.MovieDatabaseClient
{
    using System;
    using System.Configuration;
    using System.Linq;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using MoviePoker.Data.MovieDatabaseClient;

    /// <summary>
    /// Tests the interface of the MovieDatabaseClient library
    /// </summary>
    [TestClass]
    public class Test
    {
        /*
         * The use of the application configuration file in these tests depends on the post build event, 
         * copy /Y $(ProjectDir)app.config $(TargetPath).config 
         * 
         */

        const string Space = " ";
        const string ValidMovieIdentifierApplicationSettingKey = "validMovieIdentifier";
        const string ValidMovieTitleApplicationSettingKey = "validMovieTitle";
        const string ValidActorIdentifierApplicationSettingKey = "validActorIdentifier";
        const string ValidActorDirectorIdentifierApplicationSettingKey = "validActorDirectorIdentifier";
        const string ValidDirectorIdentifierApplicationSettingKey = "validDirectorIdentifier";

        public Test()
        {
        }

        private TestContext testContextInstance;

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestFindingMovieByTitle()
        {
            MovieDatabaseClientSection movieDatabaseClientConfigurationSection = 
                ConfigurationManager.GetSection(MovieDatabaseClientSection.SectionName) as MovieDatabaseClientSection;

            using(IMovieDatabaseClient movieDatabaseClient = ClientFactory.Create(
                movieDatabaseClientConfigurationSection.Address,
                movieDatabaseClientConfigurationSection.UserKey))
            {
                MovieSearchResult[] results =
                    movieDatabaseClient.EndFindMovieByTitle(
                        movieDatabaseClient.BeginFindMovieByTitle(
                            ConfigurationManager.AppSettings[Test.ValidMovieTitleApplicationSettingKey], null));

                CollectionAssert.AllItemsAreNotNull(results);
                CollectionAssert.AllItemsAreInstancesOfType(results, typeof(MovieSearchResult));
                Assert.IsTrue(results.Length >= 4);
                CollectionAssert.AllItemsAreNotNull(
                    (
                        from movieSearchResult in results
                        select movieSearchResult.Identifier
                    ).ToArray());
                CollectionAssert.AllItemsAreUnique(
                    (
                        from movieSearchResult in results
                        select movieSearchResult.Identifier
                    ).ToArray());
                CollectionAssert.AllItemsAreNotNull(
                    (
                        from movieSearchResult in results
                        select movieSearchResult.ReleaseDate
                    ).ToArray());
                CollectionAssert.AllItemsAreUnique(
                    (
                        from movieSearchResult in results
                        select movieSearchResult.ReleaseDate
                    ).ToArray());

                results =
                    movieDatabaseClient.EndFindMovieByTitle(
                        movieDatabaseClient.BeginFindMovieByTitle(Guid.NewGuid().ToString(), null));
                Assert.IsTrue(results.Length <= 0);
            }
        }

        [TestMethod]
        public void TestFindingMostSuccessfulMovieByTitle()
        {
            MovieDatabaseClientSection movieDatabaseClientConfigurationSection =
                ConfigurationManager.GetSection(MovieDatabaseClientSection.SectionName) as MovieDatabaseClientSection;

            using (IMovieDatabaseClient movieDatabaseClient = ClientFactory.Create(
                movieDatabaseClientConfigurationSection.Address,
                movieDatabaseClientConfigurationSection.UserKey))
            {
                string movieTitle = ConfigurationManager.AppSettings[Test.ValidMovieTitleApplicationSettingKey];
                Movie movie =
                    movieDatabaseClient.EndFindMostSuccessfulMovieByTitle(
                        movieDatabaseClient.BeginFindMostSuccessfulMovieByTitle(movieTitle, null));

                Assert.IsNotNull(movie.Identifier);
                Assert.IsNotNull(movie.Title);
                StringAssert.Equals(movie.Title, movieTitle);
                Assert.IsNotNull(movie.ReleaseDate.Value);
                Assert.IsNotNull(movie.Revenue.Value);
                Assert.IsTrue(movie.DirectorIdentifiers.Count > 0);
                Assert.IsTrue(movie.ActorIdentifiers.Count > 0);
                CollectionAssert.AllItemsAreNotNull(movie.DirectorIdentifiers);
                CollectionAssert.AllItemsAreNotNull(movie.ActorIdentifiers);
                CollectionAssert.AllItemsAreUnique(movie.DirectorIdentifiers);
                CollectionAssert.AllItemsAreUnique(movie.ActorIdentifiers);
            }
        }

        [TestMethod]
        public void TestGettingActor()
        {
            MovieDatabaseClientSection movieDatabaseClientConfigurationSection =
                ConfigurationManager.GetSection(MovieDatabaseClientSection.SectionName) as MovieDatabaseClientSection;

            using (IMovieDatabaseClient movieDatabaseClient = ClientFactory.Create(
                movieDatabaseClientConfigurationSection.Address,
                movieDatabaseClientConfigurationSection.UserKey))
            {
                string personIdentifier = ConfigurationManager.AppSettings[Test.ValidActorIdentifierApplicationSettingKey];
                Person person = movieDatabaseClient.EndGetPerson(
                    movieDatabaseClient.BeginGetPerson(personIdentifier, null));

                Assert.IsNotNull(person.Identifier);
                StringAssert.Equals(person.Identifier, personIdentifier);
                Assert.IsNotNull(person.Name);
                Assert.IsTrue(person.ActingCreditMovieIdentifiers.Count > 0);
                CollectionAssert.AllItemsAreNotNull(person.ActingCreditMovieIdentifiers);
                CollectionAssert.AllItemsAreUnique(person.ActingCreditMovieIdentifiers);
            }
        }

        [TestMethod]
        public void TestGettingActorDirector()
        {
            MovieDatabaseClientSection movieDatabaseClientConfigurationSection =
                ConfigurationManager.GetSection(MovieDatabaseClientSection.SectionName) as MovieDatabaseClientSection;

            using (IMovieDatabaseClient movieDatabaseClient = ClientFactory.Create(
                movieDatabaseClientConfigurationSection.Address,
                movieDatabaseClientConfigurationSection.UserKey))
            {
                string personIdentifier = ConfigurationManager.AppSettings[Test.ValidActorDirectorIdentifierApplicationSettingKey];
                Person person = movieDatabaseClient.EndGetPerson(
                    movieDatabaseClient.BeginGetPerson(personIdentifier, null));

                Assert.IsNotNull(person.Identifier);
                StringAssert.Equals(person.Identifier, personIdentifier);
                Assert.IsNotNull(person.Name);

                Assert.IsTrue(person.ActingCreditMovieIdentifiers.Count > 0);
                CollectionAssert.AllItemsAreNotNull(person.ActingCreditMovieIdentifiers);
                CollectionAssert.AllItemsAreUnique(person.ActingCreditMovieIdentifiers);

                Assert.IsTrue(person.DirectingCreditMovieIdentifiers.Count > 0);
                CollectionAssert.AllItemsAreNotNull(person.DirectingCreditMovieIdentifiers);
                CollectionAssert.AllItemsAreUnique(person.DirectingCreditMovieIdentifiers);
            }
        }

        [TestMethod]
        public void TestGettingDirector()
        {
            MovieDatabaseClientSection movieDatabaseClientConfigurationSection =
                ConfigurationManager.GetSection(MovieDatabaseClientSection.SectionName) as MovieDatabaseClientSection;

            using (IMovieDatabaseClient movieDatabaseClient = ClientFactory.Create(
                movieDatabaseClientConfigurationSection.Address,
                movieDatabaseClientConfigurationSection.UserKey))
            {
                string personIdentifier = ConfigurationManager.AppSettings[Test.ValidDirectorIdentifierApplicationSettingKey];
                Person person = movieDatabaseClient.EndGetPerson(
                    movieDatabaseClient.BeginGetPerson(personIdentifier, null));

                Assert.IsNotNull(person.Identifier);
                StringAssert.Equals(person.Identifier, personIdentifier);
                Assert.IsNotNull(person.Name);
                Assert.IsTrue(person.DirectingCreditMovieIdentifiers.Count > 0);
                CollectionAssert.AllItemsAreNotNull(person.DirectingCreditMovieIdentifiers);
                CollectionAssert.AllItemsAreUnique(person.DirectingCreditMovieIdentifiers);
            }
        }

        [TestMethod]
        public void TestGettingMovie()
        {
            MovieDatabaseClientSection movieDatabaseClientConfigurationSection =
                ConfigurationManager.GetSection(MovieDatabaseClientSection.SectionName) as MovieDatabaseClientSection;

            using (IMovieDatabaseClient movieDatabaseClient = ClientFactory.Create(
                movieDatabaseClientConfigurationSection.Address,
                movieDatabaseClientConfigurationSection.UserKey))
            {
                string movieIdentifier = ConfigurationManager.AppSettings[Test.ValidMovieIdentifierApplicationSettingKey];
                Movie movie = movieDatabaseClient.EndGetMovie(
                    movieDatabaseClient.BeginGetMovie(movieIdentifier, null));

                Assert.IsNotNull(movie.Identifier);
                StringAssert.Equals(movie.Identifier, movieIdentifier);
                Assert.IsNotNull(movie.Title);
                Assert.IsNotNull(movie.ReleaseDate.Value);
                Assert.IsNotNull(movie.Revenue.Value);
                Assert.IsTrue(movie.DirectorIdentifiers.Count > 0);
                Assert.IsTrue(movie.ActorIdentifiers.Count > 0);
                CollectionAssert.AllItemsAreNotNull(movie.DirectorIdentifiers);
                CollectionAssert.AllItemsAreNotNull(movie.ActorIdentifiers);
                CollectionAssert.AllItemsAreUnique(movie.DirectorIdentifiers);
                CollectionAssert.AllItemsAreUnique(movie.ActorIdentifiers);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestGettingBadMovie()
        {
            MovieDatabaseClientSection movieDatabaseClientConfigurationSection =
                ConfigurationManager.GetSection(MovieDatabaseClientSection.SectionName) as MovieDatabaseClientSection;

            using (IMovieDatabaseClient movieDatabaseClient = ClientFactory.Create(
                movieDatabaseClientConfigurationSection.Address,
                movieDatabaseClientConfigurationSection.UserKey))
            {
                string movieIdentifier = ConfigurationManager.AppSettings[Test.ValidMovieIdentifierApplicationSettingKey];
                Movie movie = movieDatabaseClient.EndGetMovie(
                    movieDatabaseClient.BeginGetMovie(Guid.NewGuid().ToString(), null));
                Assert.IsNull(movie);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestGettingBadPerson()
        {
            MovieDatabaseClientSection movieDatabaseClientConfigurationSection =
                ConfigurationManager.GetSection(MovieDatabaseClientSection.SectionName) as MovieDatabaseClientSection;

            using (IMovieDatabaseClient movieDatabaseClient = ClientFactory.Create(
                movieDatabaseClientConfigurationSection.Address,
                movieDatabaseClientConfigurationSection.UserKey))
            {
                string personIdentifier = ConfigurationManager.AppSettings[Test.ValidActorIdentifierApplicationSettingKey];
                Person person = movieDatabaseClient.EndGetPerson(
                    movieDatabaseClient.BeginGetPerson(personIdentifier, null));
                Assert.IsNull(person);
            }
        }
    }
}
