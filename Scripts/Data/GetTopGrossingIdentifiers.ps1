pushd .

cd ..\..\Application\binaries

[System.IO.Directory]::SetCurrentDirectory((Convert-Path (Get-Location -PSProvider FileSystem)))
[System.Console]::WriteLine([System.IO.Directory]::GetCurrentDirectory())
$bindingFlags = [System.Reflection.BindingFlags]::Default
$assembly = [System.Reflection.Assembly]::LoadFrom("MoviePoker.Data.MovieDatabase.dll")
$dependentAssembly = [System.Reflection.Assembly]::LoadFrom("MoviePoker.Management.dll")
[string[]] $arguments = "http://api.themoviedb.org/2.1/","0df1b9eda9888caf0e804477a41f968f"
$movieDatabaseClient = $assembly.CreateInstance("MoviePoker.Data.MovieDatabaseClient.Client",$false,$bindingFlags,$null,$arguments,[System.Globalization.CultureInfo]::InvariantCulture,$null)
[System.Console]::WriteLine($assembly)
[System.Console]::WriteLine($dependentAssembly)
[System.Console]::WriteLine($movieDatabaseClient)
	
$readConnection = New-Object System.Data.SqlClient.SqlConnection "Data Source=(local);Initial Catalog=MoviePoker;Integrated Security=SSPI;"
$writeConnection = New-Object System.Data.SqlClient.SqlConnection "Data Source=(local);Initial Catalog=MoviePoker;Integrated Security=SSPI;"
$readConnection.Open();
$writeConnection.Open();

$readCommand = New-Object System.Data.SqlClient.SqlCommand("SELECT DISTINCT [m].[Title] FROM [mp].[Movie] AS [m] WHERE [TopGrossing] = 1;",$readConnection)

$reader = $readCommand.ExecuteReader() 

while($reader.Read())
{
		$title = $reader[0].Trim()
		[System.Console]::WriteLine($title)
		$movie = $null
		$movie = $movieDatabaseClient.FindMostSuccessfulMovieByTitle($title)
		if(($movie -ne $null) -and ($movie.Identifier -ne $null))
		{
			[System.Console]::WriteLine("Assigning the identifier of the movie, " + $title);
			$updateCommand = New-Object System.Data.SqlClient.SqlCommand("UPDATE [mp].[Movie] SET [TheMovieDBIdentifier] = @identifier WHERE [Title] = @title;",$writeConnection)
			$updateCommand.Parameters.Add("@identifier",[System.Data.SqlDbType]::NVarChar,$movie.Identifier.Length).Value = $movie.Identifier
			$updateCommand.Parameters.Add("@title",[System.Data.SqlDbType]::NVarChar,$title.Length).Value = $title
			$updateCommand.ExecuteNonQuery();
		}
		
}

$reader.Dispose()
$readCommand.Dispose()
$readConnection.Close()
	
	
	
popd 
	
