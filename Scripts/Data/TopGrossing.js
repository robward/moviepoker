WScript.Echo("Executing ...")

WScript.Echo("Processing the adjusted gross file ... ");
var shell = new ActiveXObject("WScript.Shell");
var script = shell.Exec("cscript AdjustedGross.js");
while (script.Status == 0)
{
	WScript.Sleep(100);
}

WScript.Echo("Processing the unadjusted gross file ... ");
script = shell.Exec("cscript UnAdjustedGross.js");
while (script.Status == 0)
{
	WScript.Sleep(100);
}

WScript.Echo("Creating the top grossing movies file ... ");
var fileSystem = new ActiveXObject("Scripting.FileSystemObject");
var fileName = "../../Data/TopGrossAdjusted_Clean.TXT";
var inputFile = fileSystem.OpenTextFile(fileName, 1);
fileName = "../../Data/TopGrossingMovies.TXT";
var outputFile = fileSystem.OpenTextFile(fileName, 2, true);
var record;
while (!(inputFile.AtEndOfStream))
{
	record = inputFile.ReadLine();
	if(record != "")
	{
		WScript.Echo(record);
  	outputFile.WriteLine("('" + record.replace(/^\s*/, "").replace(/\s*$/, "") + "'),");
  }	
}
inputFile.Close();

fileName = "../../Data/TopGrossUnAdjusted_Clean.TXT";
inputFile = fileSystem.OpenTextFile(fileName, 1);
while (!(inputFile.AtEndOfStream))
{
	record = inputFile.ReadLine();
	if(record != "")
	{
		WScript.Echo(record);
  	outputFile.WriteLine("('" + record.replace(/^\s*/, "").replace(/\s*$/, "") + "'),");
  }	
}
outputFile.Close();


