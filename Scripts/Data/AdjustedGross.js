WScript.Echo("Executing ...");
var fileSystem = new ActiveXObject("Scripting.FileSystemObject");
var fileName = "../../Data/TopGrossAdjusted.TXT";
var inputFile = fileSystem.OpenTextFile(fileName, 1);
fileName = "../../Data/TopGrossAdjusted_Clean.TXT";
var outputFile = fileSystem.OpenTextFile(fileName, 2, true);
var record;
var regularExpression = new RegExp(".*\\(","ig")
while (!(inputFile.AtEndOfStream))
{
	record = inputFile.ReadLine();
	if(record != "")
	{
		record = new String(record.match(regularExpression));
		if((record)&&(record != ""))
		{
			record = record.substring(0,record.length-1);
  		WScript.Echo(record);
  		outputFile.WriteLine(record);
  	}	
  }	
}
inputFile.Close();
outputFile.Close();