pushd .

cd ..\..\Application\binaries

[System.IO.Directory]::SetCurrentDirectory((Convert-Path (Get-Location -PSProvider FileSystem)))
[System.Console]::WriteLine([System.IO.Directory]::GetCurrentDirectory())
$bindingFlags = [System.Reflection.BindingFlags]::Default
$assembly = [System.Reflection.Assembly]::LoadFrom("MoviePoker.Data.MovieDatabase.dll")
$dependentAssembly = [System.Reflection.Assembly]::LoadFrom("MoviePoker.Management.dll")
[string[]] $arguments = "http://api.themoviedb.org/2.1/","0df1b9eda9888caf0e804477a41f968f"
$movieDatabaseClient = $assembly.CreateInstance("MoviePoker.Data.MovieDatabaseClient.Client",$false,$bindingFlags,$null,$arguments,[System.Globalization.CultureInfo]::InvariantCulture,$null)
	
$readConnection = New-Object System.Data.SqlClient.SqlConnection "Data Source=(local);Initial Catalog=MoviePoker;Integrated Security=SSPI;"
$writeConnection = New-Object System.Data.SqlClient.SqlConnection "Data Source=(local);Initial Catalog=MoviePoker;Integrated Security=SSPI;"
$readConnection.Open();
$writeConnection.Open();

$readCommand = New-Object System.Data.SqlClient.SqlCommand("SELECT DISTINCT [m].[TheMovieDBIdentifier] FROM [mp].[Movie] AS [m] WHERE [TopGrossing] = 1;",$readConnection)

$reader = $readCommand.ExecuteReader() 

while($reader.Read())
{
		$identifier = $reader[0].Trim()
		[System.Console]::WriteLine($identifier)
		$movie = $movieDatabaseClient.GetMovie($identifier)
		if(($movie -ne $null) -and ($movie.Revenue -ne $null))
		{
			[System.Console]::WriteLine("Assigning the revenue of the movie, " + $identifier);
			$updateCommand = New-Object System.Data.SqlClient.SqlCommand("UPDATE [mp].[Movie] SET [WorldWideRevenue] = @revenue WHERE [TheMovieDBIdentifier] = @identifier;",$writeConnection)
			$updateCommand.Parameters.Add("@identifier",[System.Data.SqlDbType]::NVarChar,$movie.Identifier.Length).Value = $identifier
			$updateCommand.Parameters.Add("@revenue",[System.Data.SqlDbType]::BigInt,$null).Value = $movie.Revenue
			$updateCommand.ExecuteNonQuery();
		}
		
}

$reader.Dispose()
$readCommand.Dispose()
$readConnection.Close()
	
	
	
popd 
	
